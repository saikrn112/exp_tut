# # ----------
# # User Instructions:
# # 
# # Define a function, search() that returns a list
# # in the form of [optimal path length, row, col]. For
# # the grid shown below, your function should output
# # [11, 4, 5].
# #
# # If there is no valid path from the start point
# # to the goal, your function should return the string
# # 'fail'
# # ----------

# # Grid format:
# #   0 = Navigable space
# #   1 = Occupied space

# grid = [[0, 0, 1, 0, 0, 0],
#         [0, 0, 1, 0, 0, 0],
#         [0, 0, 0, 0, 1, 0],
#         [0, 0, 1, 1, 1, 0],
#         [0, 0, 0, 0, 1, 0]]
# init = [0, 0]
# goal = [len(grid)-1, len(grid[0])-1]
# cost = 1

# delta = [[-1, 0], # go up
#          [ 0,-1], # go left
#          [ 1, 0], # go down
#          [ 0, 1]] # go right

# delta_name = ['^', '<', 'v', '>']

# def search(grid,init,goal,cost):
#     # ----------------------------------------
#     # insert code here
#     # ----------------------------------------
#     node = grid[init[0],init[1]]
#     for i in range(len(grid)):
#     	for j in range(len(grid(i))):
#     		gval[i,j] = 0

#     while stat == 'success' or stat == 'fail':

#     	#select - given a set of nodes it should select the one with lowest g value
#     	#expand - given a node it should expand and give valid neighbours
#     	#track - track the lowest g value selected till now.
#     	nodes = expand(node)
#     	node  = select(nodes)
#     	stat = check(node,goal)
    
#     return path

# def select(nodes):
# 	global gval
# 	lowest = gval(nodes[0][0],nodes[0][1])
# 	k = 0
# 	for i in range(len(nodes)):
# 		if lowest >= gval(nodes[0][0],nodes[0][1]):
# 			lowest = gval(nodes[0][0],nodes[0][1])
# 			k = i
# 	return nodes[k]

# def expand(node):
# 	[i,j] = node
# 	nodes = []
# 	global g, grid
# 	g += 1
# 	if i > 0 and j > 0 and i < len(grid) -1 and j < len(grid(len(grid)-1)) - 1:
# 		if grid[i-1,j] == 0:
# 			nodes.append([i-1,j])
# 		if grid[i,j-1] == 0:
# 			nodes.append([i,j-1])
# 		if grid[i+1,j] == 0:
# 			nodes.append([i+1,j])
# 		if grid[i,j+1] == 0:
# 			nodes.append([i,j+1])
# 	elif i ==0:
# 		if j == len(grid(len(grid)-1))-1:
# 			if grid[i,j-1] == 0:
# 				nodes.append([i,j-1])
# 			if grid[i+1,j] == 0:
# 				nodes.append([i+1,j])
# 		elif j == 0:
# 			if grid[i+1,j] == 0:
# 				nodes.append([i+1,j])
# 			if grid[i,j+1] == 0:
# 				nodes.append([i,j+1])
# 	elif i ==len(grid)-1:
# 		if j == len(grid(len(grid)-1))-1:
# 			if grid[i-1,j] == 0:
# 				nodes.append([i-1,j])
# 			if grid[i,j-1] == 0:
# 				nodes.append([i,j-1])
# 		elif j == 0:
# 			if grid[i-1,j] == 0:
# 				nodes.append([i-1,j])
# 			if grid[i,j+1] == 0:
# 				nodes.append([i,j+1])
# 	for i in range(len(nodes)):
# 		gval[nodes[i][0],nodes[i][1]] = g
# 	return nodes

# def check(node,goal):
# 	if node[0] == goal[0] and node[1] == goal[1]:
# 		return 'success'
# 	else: 
# 		return 'wait'


# -----------
# User Instructions
#
# Define a function smooth that takes a path as its input
# (with optional parameters for weight_data, weight_smooth,
# and tolerance) and returns a smooth path. The first and 
# last points should remain unchanged.
#
# Smoothing should be implemented by iteratively updating
# each entry in newpath until some desired level of accuracy
# is reached. The update should be done according to the
# gradient descent equations given in the instructor's note
# below (the equations given in the video are not quite 
# correct).
# -----------

from copy import deepcopy

# thank you to EnTerr for posting this on our discussion forum
def printpaths(path,newpath):
    for old,new in zip(path,newpath):
        print '['+ ', '.join('%.3f'%x for x in old) + \
               '] -> ['+ ', '.join('%.3f'%x for x in new) +']'

# Don't modify path inside your function.
path = [[0, 0],
        [0, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [4, 2],
        [4, 3],
        [4, 4]]

def smooth(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.000001):

    # Make a deep copy of path into newpath
    newpath = deepcopy(path)

    #######################
    ### ENTER CODE HERE ###
    #######################
    Xflag = 1 
    Yflag = 1
    while (Xflag != len(path)-1): 
        for i in range(1,len(path)-1):
            old = newpath[i][0]
            newpath[i][0] = newpath[i][0] + weight_data*(newpath[i][0] - path[i][0]) + weight_smooth*(newpath[i-1][0] + newpath[i+1][0] - 2*newpath[i][0])
            if abs(newpath[i][0]-old< tolerance):
                Xflag += 1
                print 'x''s'
    while ( Yflag != len(path)-1	):
        for i in range(1,len(path)-1):
            old = newpath[i][1]
            newpath[i][1] = newpath[i][1] + weight_data*(newpath[i][1] - path[i][1]) + weight_smooth*(newpath[i-1][1] + newpath[i+1][1] - 2*newpath[i][1])
            if abs(newpath[i][1]-old< tolerance):
                Yflag += 1
                print 'y''s'   
    return newpath # Leave this line for the grader!

printpaths(path,smooth(path))
