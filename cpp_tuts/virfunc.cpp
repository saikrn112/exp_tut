#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Person {
    public:
    int age;
    string name;
    virtual void getdata()=0;
    virtual void putdata()=0;
};
class Professor: public Person {
	static int val;
    public:

        int publications;
        int cur_id=1;
        Professor();
        void getdata(){
            cin >> name >> age >> publications;

        }
        void putdata(){
            cout << name <<" " << age <<" " << publications <<" " << cur_id << endl;
        }
};

int Professor::val=0;
Professor::Professor(){
    val+=1;
};

class Student: public Person {
    public:
        int marks[6];
        int cur_id=1;
        static int val;
        Student();
        void getdata(){
            cin >> name >> age;
            for(int i =0;i<6;i++)cin >> marks[i];
        }
        void putdata(){
            cout << " " << name << " " << age << " ";
            for(int i=0;i<6;i++)cout << marks[i] << " ";
            cout << cur_id << endl;
        }
};
int Student::val=0;
Student::Student(){
    val+=1;
}

int main(){

    int n, val;
    cin>>n; //The number of objects that is going to be created.
    Person *per[n];

    for(int i = 0;i < n;i++){

        cin>>val;
        if(val == 1){
            // If val is 1 current object is of type Professor
            per[i] = new Professor;

        }
        else per[i] = new Student; // Else the current object is of type Student

        per[i]->getdata(); // Get the data from the user.

    }

    for(int i=0;i<n;i++)
        per[i]->putdata(); // Print the required output for each object.

    return 0;

}
