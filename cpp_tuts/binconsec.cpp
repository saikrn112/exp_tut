#include <list>
#include <iostream>

using namespace std;

void print_list(list<int> mylist){
    for(int i=0;i<mylist.size();i++){
        cout << mylist.front();
        mylist.pop_front();
    }
}

void onecounter(list<int> mylist){
    cout << "I'm inside " << endl;
    int counter=0,adcounter=0;
    while(!mylist.empty()){
        counter=0;
        cout << "stuck 1" << endl;
        while(mylist.front()==1){
            mylist.pop_front();
            cout << "stuck 2" << endl;
            counter++;
            if(mylist.empty())break;
        }
        mylist.pop_front();
        if(adcounter<counter)adcounter=counter;
    }
    cout << "I'm at edge " << endl;
    cout << adcounter << endl;
}

int main(){
    int n=0,remainder; 
    list<int> mylist;
    cin >> n;
    while (n>0){
        remainder = n%2;
        n/=2;
        mylist.push_front(remainder);
    }
    onecounter(mylist);
    return 0;
}
