// This code is to test if setbase(2) works for cin (TEST1)
// This code is to take string(which contains binary) and convert to binary
#include <iostream>
#include <iomanip>
#include <stdlib.h>
using namespace std;
int main(){
 	
 	/* TEST 1 *//*
	int n;
 	cin >> setbase(16) >> n; // this will take input as specified in setbase()
 	cin >> n;
 	cout << setbase(16) << n << endl;
 	cout << setbase(2) << n << endl;
 	*/

 	/* TEST 2 */
 	int n; 
 	string a;
 	cin >> a;
 	//for(int i=0; i<a.size();i++)
 	 cout<<atoi(a);
 	cout << endl;

 	return 0;
 }