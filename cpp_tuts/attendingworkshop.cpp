#include<bits/stdc++.h>

using namespace std;
//Define the structs Workshops and Available_Workshops.
//Implement the functions initialize and CalculateMaxWorkshops
#include <vector>
struct Workshop{
    int start_time; 
    int duration;
    int end_time = start_time + duration;
};
struct Available_Workshops{
    int n;
    //std::vector<Workshop> workshop;
    //workshop.resize(n);
    Workshop* workshop = new Workshop[n];
};

Available_Workshops* initialize(int start_time[],int duration[],int n){
    // cout << "inside aws 1" << endl;
    Available_Workshops* aws = new Available_Workshops;
    aws->n = n;
    // cout << "inside aws" << endl;
    for(int i=0;i<n;i++){
        // cout << "aws for" << endl;
        aws->workshop[i].start_time = start_time[i];
        aws->workshop[i].duration = duration[i];
    }
    return aws;
}
int CalculateMaxWorkshops(Available_Workshops* ptr){
    int N,counter,startj,endi,k,j; 
    int n = ptr->n;
    for(int i=0;i<n;i++){
        cout << "inside for" << endl;
        j=i;
        k=i+1;
        counter = 0;
        while(j<n){
            cout << "inside while" << endl;
            startj = ptr->workshop[j].start_time;
            endi = ptr->workshop[k].end_time;
            if(endi < startj){
                counter++;
                cout << "counter value: " << counter << "k value: " << k <<  endl;
                k = j;
                j++;
            }
            else j++;
            cout << "j value: " << j << endl;
        }
        if(counter > N) N = counter;
        cout << "N value: " << N << endl;
    }
    return N;
}

int main(int argc, char *argv[]) {
    int n; // number of workshops
    cin >> n;
    // create arrays of unknown size n
    int* start_time = new int[n];
    int* duration = new int[n];
    cout << "step 1" << endl;
    for(int i=0; i < n; i++){
        cin >> start_time[i];
        cout << "step 2" << endl;
    }
    for(int i = 0; i < n; i++){
        cin >> duration[i];
        cout << "step 3" << endl;
    }

    Available_Workshops * ptr;
    cout << "step 4" << endl;
    ptr = initialize(start_time,duration, n);
    cout << CalculateMaxWorkshops(ptr) << endl;
    return 0;
}
