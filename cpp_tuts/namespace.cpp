#include <bits/stdc++.h>
using namespace std;

namespace fusion{
namespace n1{
  class EKF{
  public:
    EKF();
    ~EKF();
  };
}
}
namespace fusion{
namespace n2{
  class UKF: public n1::EKF{
  public:
    UKF();
    ~UKF();
  };
}
}

int main(){
  return 0;
}
