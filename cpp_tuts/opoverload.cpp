//This is taken from hackerrank c++ overload operators question
#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;
class Matrix{
public:
	std::vector<vector<int> > a;
	Matrix operator+(const Matrix& B){
		cout << "Step 3.1: Inside overloaded operator" << endl;
		Matrix C;
		C.a.resize(this->a.size());
		for (int i =0;i<this->a.size();i++)C.a[i].resize(this->a[i].size());
		cout << C.a[1][1] << endl;
		for(int i=0;i<this->a.size();i++){
			cout << "Step 3.2: Get the size of LHS matrix rows" << endl;
			cout << "row size of matrix is " << B.a[i].size() << endl;
			cout << "Step 3.3: Adding" << endl << " coloumn size: " << B.a[i].size() << endl;
			for(int j = 0;j<this->a[i].size();j++){
				// cout << "a[i][j]" << this->a[i][j] << " ";
				C.a[i][j]=this->a[i][j] + B.a[i][j];
			}
			cout << endl;
		}
		return C;
	}
};

int main(){
	int N; cin >> N; //This line defines number of test cases
	cout << "Step 1: cases" << endl;
	for(int i=0;i<N;i++){ // For each test case
		int n=0, m=0;
		cin >> n >> m; // n -- rows and m -- coloumns
		Matrix x; // defining new matrix x, y and result (x+y) for each test case
		Matrix y;
		Matrix result; 
		cout << "Step 2: Matrix 1" << endl;
		for(int k=0;k<n;k++){// for each row wise in test case
			vector<int> b; // temporary vector for each row 
			int num;
			for(int j=0;j<m;j++) {
				cin >> num;
				b.push_back(num);// creating a vector of row
			}
			x.a.push_back(b);// pushing that row from the back so that original matrix is preserved
		}   
		cout << "Step 2: Matrix 2" << endl;
		for(int k=0;k<n;k++){// for each row wise in test case
			vector<int> b;
			int num;
			for(int j=0;j<m;j++) {// similarly for another matrix in current test case
				cin >> num;
				b.push_back(num);
			}
			y.a.push_back(b);	
		}
		cout << "Step 3: Before Adding " << endl;
		result = x + y; 
		cout << "Step 3: After Adding " << endl;
		for(int i=0;i<n;i++){
			for (int j=0;j<m;j++){
				cout << result.a[i][j] << " ";
			}
			cout<< endl;
		}	
	} 
	return 0;
}