#include <iostream>
#include <queue>
using namespace std;

typedef struct node
{
   int data;
   node * left;
   node * right;
}node;


node * insert(node * root, int value) {
    if(!root){
        node* node1 = new node;
        node1->data = value;
        node1->left = NULL;
        node1->right = NULL;
        return node1;
    }

    //cout << root->data << " ";
    node* node2 = root;
    if(value> node2->data){
        node2->right = insert(node2->right,value);
    } else if(value < node2->data){
        node2->left = insert(node2->left, value);
    }

    return node2;
}

void levelOrder(node * root) {
    if(!root) return;
    queue<node*> nodeQueue;
    nodeQueue.push(root);
    node* tmp;
    while(!nodeQueue.empty()){
        tmp = nodeQueue.front();
        cout << tmp->data << " ";
        if(tmp->left) nodeQueue.push(tmp->left);
        if(tmp->right) nodeQueue.push(tmp->right);
        nodeQueue.pop();
    }
}

int main(){
  int size = 4;
  node* tmp=NULL;
  for(int i=0; i<size; i++){
    int t; cin >> t;
    tmp = insert(tmp,t );
  }
  levelOrder(tmp);
  return 0;
}
