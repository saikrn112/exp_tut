    /* **********************************
     * My ACM header - Simplified Version
     *  lz96@foxmail.com
     * Copy to the beginning of your program
     * **********************************/
    #define T_ int  //Type of iterated variable. You can change it to long long.
    #define fuck(s_) {cout<<s_<<endl;return 0;} //print s_ and terminate the program:   fuck("NO");
    /*************************
     * Iterations
     * re：3 parameters: variable, initial value, final value
     * rep: 2 parameters：variable, final value/initial value
     * suffix 2/3：2/3 dimensions
     * suffix 0：initial/final value is 0, otherwise 1
     * prefix r: reversed
     * Why I use a fixed type T_ instead of "auto" or " __typeof(x_)__"? Because  many OJs don't provide c++11 or gcc. 
     * ***********************/
    #define printarr(a_,x_) rep(i_,x_)cout<<a_[i_]<<" ";cout<<endl; //print 1d array.         e.g to print a[1] ~ a[5]:  printarr(a,5)
    #define printarr0(a_,x_) rep0(i_,x_)cout<<a_[i_]<<" ";cout<<endl; // print a[0] ~ a[4]:    printarr0(a,5)
    #define printarr2(a_,x_,y_) rep(i_,x_){rep(j_,y_)cout<<a[i_][j_]<<' ';cout<<endl;} //print 2d array
    #define printarr20(a_,x_,y_) rep0(i_,x_){rep0(j_,y_)cout<<a[i_][j_]<<' ';cout<<endl;} //print 2d array from a[0]
    #define rep2o(a_,b_,n_) rep(a_,n_) re(b_,a_+1,n_) // for(T_ a_=1;a_<=n_;++a_) for(T_ b_=a_+1;b_<=n_;++b_)
    #define rep20o(a_,b_,n_) rep0(a_,n_) re0(b_,a_+1,n_) // for(T_ a_=0;a_<n_;++a_) for(T_ b_=a_+1;b_<n_;++b_)
    #define rep2(a_,b_,p_,q_) rep(a_,p_) rep(b_,q_)// for(T_ a_=1;a_<=n_;++a_) for(T_ b_=1;b_<=n_;++b_)
    #define rep20(a_,b_,p_,q_) rep0(a_,p_) rep0(b_,q_) // for(T_ a_=0;a_<n_;++a_) for(T_ b_=0;b_<n_;++b_)
    #define rrep2(a_,b_,p_,q_) rrep(a_,p_) rrep(b_,q_)
    #define rrep20(a_,b_,p_,q_) rrep0(a_,p_) rrep0(b_,q_)
    #define rep3(a_,b_,c_,p_,q_,r_) rep(a_,p_) rep(b_,q_) rep(c_,r_)
    #define rep30(a_,b_,c_,p_,q_,r_) rep0(a_,p_) rep0(b_,q_) rep0(c_,r_)
    #define rrep3(a_,b_,c_,p_,q_,r_) rrep(a_,p_) rrep(b_,q_) rrep(c_,r_)
    #define rrep30(a_,b_,c_,p_,q_,r_) rrep0(a_,p_) rrep0(b_,q_) rrep0(c_,r_)
    #define rep(a_,x_) re(a_,1,x_) //rep(i,5)   == for(T_ i=1;i<=5;++i)
    #define rep0(a_,x_) re0(a_,0,x_) //rep0(i,5)   == for(T_ i=0;i<5;++i)
    #define rrep(a_,x_) rre(a_,x_,1) //rrep(i,5)  ==  for(T_ i=5;i>=1;--i)
    #define rrep0(a_,x_) rre(a_,x_-1,0) //rrep0(i,5)  ==  for(T_ i=4;i>=0;--i)
    #define re(a_,s_,t_) for(T_ a_=s_;a_<=t_;++a_) // re(i,2,4)  ==   for(T_ i=2;i<=4;++i)
    #define re0(a_,s_,t_) for(T_ a_=s_;a_<t_;++a_) // re0(i,2,4)  ==   for(T_ i=2;i<4;++i)
    #define rre(a_,s_,t_) for(T_ a_=s_;a_>=t_;--a_)
    #define rre0(a_,s_,t_) for(T_ a_=s_;a_>t_;--a_)
    #define repit(a_,c_) for(__typeof__(a_.begin()) c_=a_.begin();c_!=a_.end();++c_) // GCC ONLY：repit(container,iterator)
    #define nosync std::ios::sync_with_stdio(false) //Add to the first line of main() to speed up your io, but scanf/printf is unavailable after this statement
    #define DE false //debugging?
    #define de(s_) if(DE){s_ } //  de(statement that should only execute in debugging mode)     de(cout<<n<<m<<endl;)
    #include <iostream>
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
    #include <string>
    #include <cmath>
    #include <algorithm>
    #include <map>
    #include <vector>
    #include <deque>
    #include <list>
    #include <set>
    #include <numeric>
    #include <bitset>
    #include <fstream>
    #include <iomanip>
    #include <sstream>
    #include <ctime>
    #include <stack>
    using namespace std;
    const int dx4[] = {-1, 0, 1, 0};
    const int dy4[] = {0, 1, 0, -1};  //rep0(i,4)  (basex+dx4[i] , basey+dy4[i])
    const long long modb=100000007;  //  ModBase
    const long inf=0x3f3f3f3f;      // Large int
    const double oo=1e15;           //Large double
    const double eps=1e-8;          //Small double, used for computational geometry
    const double pi=acos(-1.0);     //pi
    template<typename T> void clr(T* a_,int n_=0,size_t s_=-1) {if (s_==-1) s_=sizeof(a_); memset(a_,n_,sizeof(a_[0])*s_); }  // clr(a)：Clear a to 0    clr(a,0xff)：Clear each byte of a to 0xff      clr(a,0xff,4) : Clear each byte of a[0]~a[3] to 0xff
    template<typename T> T sqr(T a_) { return a_*a_; }  //squared
    template<typename T> T cub(T a_) { return a_*a_*a_; }  //cubic
    inline T_ mf(T_ n_) {return ((n_<0)?n_+(modb<<3):n_)%modb; }  //mf(n) = n%modb with exception for minus numbers : ModFunction . You may need to change 3 to make modb<<3 large enough
    template<typename T>T max(T a_,T b_,T c_) { return max(a_,max(b_,c_)); } //max with 3 parameters
    template<typename T>T min(T a_,T b_,T c_) { return min(a_,min(b_,c_)); } //min with 3 parameters
    inline int dbcmp(double a_, double b_) { return (fabs(a_-b_)<eps)?0:(a_<b_?-1:1); } //cmp with epsilon，< : -1    == : 0    > : 1
    inline double dbdiv(T_ a_, T_ b_) { return static_cast<double>(a_) / b_; } //double div:  dbdiv(3,5)=0.6
    inline double angle(double x_, double y_) { if (x_==0.0) return y_>0?pi/2:3*pi/2; else { double t_=atan2(y_,x_); return (t_<0.0?t_+pi:t_) + y_<0?pi:0.0; }} //Compute the angle of <x_,y_> [0,2*pi)
    #pragma GCC optimize O2 //Force GCC to enable O2 optimization
    // Changed to O2 instead of O3 because *O3 on gcc 4.8-* is completely s***