// Broadcasting a Transform


#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <sensor_msgs/Imu.h>
double baseLink_X = 0.00;
double baseLink_Y = 0.00;
double baseLink_Z = 0.00;
double Quaternion_X = 0.00;
double Quaternion_Y = 0.00;
double Quaternion_Z = 0.00;
double Quaternion_W = 1.00;

ros::Publisher imupub;

void odomCallback( const::nav_msgs::Odometry::ConstPtr& msg ) {
  baseLink_X = msg->pose.pose.position.x;
  baseLink_Y = msg->pose.pose.position.y;
  baseLink_Z = msg->pose.pose.position.z;

  Quaternion_X = msg->pose.pose.orientation.x;
  Quaternion_Y = msg->pose.pose.orientation.y;
  Quaternion_Z = msg->pose.pose.orientation.z;
  // Quaternion_Y = 0;
  // Quaternion_Z = 0;
  Quaternion_W = msg->pose.pose.orientation.w;

  // ROS_INFO( "[TF_BROADCASTER]: baselink -> { (%6.4f,%6.4f,%6.4f), (%6.4f,%6.4f,%6.4f,%6.4f) }\n",
  //   baseLink_X, baseLink_Y, baseLink_Z,
  //   Quaternion_X, Quaternion_Y, Quaternion_Z, Quaternion_W );

}

void imuCallback( const::sensor_msgs::Imu::ConstPtr& msg){
  sensor_msgs::Imu out;
  out = *msg;
  out.header.frame_id = "imu";
  imupub.publish(out);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;

  ros::Rate r(50);

  // subscribe data from arduino '/odom' frame -- curde estimate of baselink
  // ros::Subscriber odomSub = n.subscribe( "/odom", 1000, odomCallback );
  ros::Subscriber odomSub = n.subscribe( "/imu_", 1000, imuCallback );

  tf::TransformBroadcaster broadcaster;

  ROS_INFO( "[TF_BROADCASTER]: Starting....\n" );

  while(n.ok()){
    broadcaster.sendTransform(
      tf::StampedTransform(
        tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.55, 0.0, 0.41)),
        ros::Time::now(),"base_link", "laser"));    /*For LiDAR scans*/
  
      broadcaster.sendTransform(
      tf::StampedTransform(
        tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.0,0.0,0.0)),
        ros::Time::now(),"base_link", "imu"));    /*For LiDAR scans*/
  
     // broadcaster.sendTransform(
     //  tf::StampedTransform(
     //    tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.1, 0.0, 0.4)),
     //    ros::Time::now(),"base_link", "cam_laser"));      /*For images as laser scan*/
     
     broadcaster.sendTransform(
      tf::StampedTransform(
        tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.0, 0.0, 0.195)),
        ros::Time::now(), "base_footprint", "base_stabilized" ));      /*Base footprint for IMU*/

     // broadcaster.sendTransform(
     //  tf::StampedTransform(
     //    tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0.0, 0.0, 0.195)),
     //    ros::Time::now(), "base_footprint", "base_link" ));      /*Base footprint for IMU*/

     // broadcaster.sendTransform(
     //  tf::StampedTransform(
     //    tf::Transform(
     //      tf::Quaternion(0,0,Quaternion_Y,Quaternion_W), 
     //      tf::Vector3(baseLink_X,baseLink_Y,baseLink_Z)),
     //    // ros::Time::now(),"odom_combined", "base_link"));      /*Base footprint for Odom_combined*/
     //    ros::Time::now(),"odom", "base_footprint"));      /*Base footprint for Odom*/
      imupub = n.advertise<sensor_msgs::Imu>("imu", 2);
    ros::spinOnce();
    r.sleep();
  }
}


/*
[ WARN] [1476185528.259573699]: Invalid Trajectory 0.000000, 0.000000, 0.640000, cost: -6.000000
[ WARN] [1476185528.259645735]: Rotation cmd in collision
[ INFO] [1476185528.259751042]: Error when rotating.
[ERROR] [1476185528.459339708]: Aborting because a valid control could not be found. Even after executing all recovery behaviors


*/