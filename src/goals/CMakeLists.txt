cmake_minimum_required(VERSION 3.2.)
project(goals)
set (CMAKE_CXX_STANDARD 11)
find_package(catkin REQUIRED COMPONENTS
  actionlib
#  gpsd_client
  move_base_msgs
  roscpp
  rospy
  message_filters
  roslib
)


catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES simple_navigation_goals
#  CATKIN_DEPENDS actionlib gpsd_client move_base_msgs roscpp rospy
#  DEPENDS system_lib
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)


include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(simple_navigation_goals src/simple_navigation_goals.cpp)
target_link_libraries(simple_navigation_goals ${catkin_LIBRARIES})


add_executable(GPSgoals src/GPSgoals.cpp)
target_link_libraries(GPSgoals ${catkin_LIBRARIES})

add_executable(waypoints src/set_goals.cpp)
target_link_libraries(waypoints ${catkin_LIBRARIES})

add_executable(GPSWaypoints src/GPSWaypoints.cpp)
target_link_libraries(GPSWaypoints ${catkin_LIBRARIES})

add_executable(GPStoUTM src/GPStoUTM.cpp)
target_link_libraries(GPStoUTM ${catkin_LIBRARIES})
