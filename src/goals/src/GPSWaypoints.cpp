#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <goals/conversions.h>
#include <fstream>
#include <queue>
#include <string>
#include <signal.h>
#include <ros/package.h>

using namespace std;
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

struct goals{
  double utm_x=0; 
  double utm_y=0; 
};

void siginthandler(int param)
{
  printf("User pressed Ctrl+C\n");
  ros::shutdown();
  exit(1);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "Waypoints",ros::init_options::NoSigintHandler);
  string name;
  char* filename = new char;
  if((argc >1))  {
    sscanf(argv[1],"%s",filename);
    name = filename;
  } else {
  name = "Waypoints.txt"; // Default filename 
  }

  std::string file = ros::package::getPath("goals");// Find the package and get the path 
  file += "/src/" + name; // Complete location of waypoints file

  /*Opening a file a storing the waypoints*/
  goals waypoint;
  string lat_s, long_s;
  double lat, longi;
  queue<goals> waypoints;
  ifstream myfile; 
  myfile.open(file); // opening the file   
  char* zone = new char;
  if (myfile.is_open()) {    
    /* Parsing the file and storing in the hashmap */ 
    while(getline(myfile,lat_s,',')){
      lat = stod(lat_s);
      getline(myfile,long_s);  longi = stod(long_s);

      gps_common::LLtoUTM(lat,longi,waypoint.utm_y,waypoint.utm_x,zone);// Converting from lat, long to UTM
      waypoints.push(waypoint);
      cout.precision(17);
      cout << "lat: " << lat << "   utm_x: " << waypoint.utm_x << endl;
      cout << "long: " << longi << "   utm_y: " << waypoint.utm_y << endl;
    }
    myfile.close();
  } else {
    cout << "Couldn't Open file" << endl; 
    return 1;
  } 

  /* Sending Goals through Move Base Action client */
  bool failed = false; // Flag for checking if reaching goal has failed or not
  MoveBaseClient ac("move_base", true); //tell the action client that we want to spin a thread by default
  signal(SIGINT, siginthandler);
  while(!ac.waitForServer(ros::Duration(5.0)))ROS_INFO("Waiting for the move_base action server to come up");   //wait for the action server to come up
  
  while(!waypoints.empty() && !failed){
    waypoint = waypoints.front();
    move_base_msgs::MoveBaseGoal goal;
    ROS_INFO_STREAM (" Sending the goal (" <<waypoint.utm_x << ","<< waypoint.utm_y<<")");
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = waypoint.utm_x;
    goal.target_pose.pose.position.y = waypoint.utm_y;
    goal.target_pose.pose.orientation.w = 1.0;
    waypoints.pop();
    ROS_INFO("Sending goal");
    ac.sendGoal(goal);

    ac.waitForResult();

    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
      ROS_INFO_STREAM (" Bot reached the specified location (" <<lat_s << ","<< long_s<<")");
    else{
      failed = true;
      ROS_INFO_STREAM ( "Bot failed to reach the location (" <<lat_s << ","<< long_s<<") for some reason");
    }
  }
  return 0;
}	