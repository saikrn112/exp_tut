#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <fstream>
#include <queue>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/package.h>
using namespace std;
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

struct goals{
  float x; 
  float y; 
};

void siginthandler(int param)
{
  printf("User pressed Ctrl+C\n");
  ros::shutdown();
  exit(1);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "Waypoints",ros::init_options::NoSigintHandler);
  
  std::string file = ros::package::getPath("goals");
  file += "/src/Waypoints.txt";
  /*Opening a file a storing the waypoints*/
  goals waypoint;
  string x_s, y_s, z_s; 
  queue<goals> waypoints;
  ifstream myfile; 
  // myfile.open("/home/bhagyasyam/OA/src/goals/src/Waypoints.txt"); // opening the file   
  myfile.open(file); // opening the file   

  if (myfile.is_open()) {    
    /* Parsing the file and storing in the hashmap */ 
    while(getline(myfile,x_s,',')){
      waypoint.x = stoi(x_s);
      getline(myfile,y_s);  waypoint.y = stoi(y_s);
      waypoints.push(waypoint);
      cout << waypoint.x << "," << waypoint.y << endl;
    }
    myfile.close();

  } else {
    cout << "Couldn't Open file" << endl; 
    return 1;
  } 

  /* Sending Goals through Move Base Action client */
  bool failed = false; // Flag for checking if reaching goal has failed or not
  MoveBaseClient ac("move_base", true); //tell the action client that we want to spin a thread by default
  signal(SIGINT, siginthandler);
  while(!ac.waitForServer(ros::Duration(5.0)))ROS_INFO("Waiting for the move_base action server to come up");   //wait for the action server to come up
  
  while(!waypoints.empty() && !failed){
    waypoint = waypoints.front();
    move_base_msgs::MoveBaseGoal goal;
    ROS_INFO_STREAM (" Sending the goal (" <<waypoint.x << ","<< waypoint.y<<")");
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = waypoint.x;
    goal.target_pose.pose.position.y = waypoint.y;
    goal.target_pose.pose.orientation.w = 1.0;
    waypoints.pop();
    ROS_INFO("Sending goal");
    ac.sendGoal(goal);

    ac.waitForResult();

    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
      ROS_INFO_STREAM (" Bot reached the nearly to the goal (" <<waypoint.x << ","<< waypoint.y<<")");
    else{
      failed = true;
      ROS_INFO_STREAM ( "The base failed to reach the goal (" <<waypoint.x << ","<< waypoint.y<<") for some reason");
    }
  }
  return 0;
}	