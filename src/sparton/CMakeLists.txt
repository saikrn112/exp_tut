cmake_minimum_required(VERSION 3.2.2)
project(sparton)
set (CMAKE_CXX_STANDARD 11)
find_package(catkin REQUIRED COMPONENTS
  sensor_msgs
  roscpp
  roslib
  # boost
)


catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES simple_navigation_goals
#  CATKIN_DEPENDS actionlib gpsd_client move_base_msgs roscpp rospy
#  DEPENDS system_lib
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)


include_directories(include ${catkin_INCLUDE_DIRS})
add_executable(sparton src/main.cpp src/boost_serial.cpp src/IMU_AHRS8.cpp)
target_link_libraries(sparton ${catkin_LIBRARIES} ${BOOST_SYSTEM_LIBRARY})
catkin_install_python(PROGRAMS 
	nodes/display_3D_visualization.py
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/nodes
)
