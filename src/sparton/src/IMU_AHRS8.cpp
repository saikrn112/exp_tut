/******
System libaries
******/

#include<iostream>


/*******
local Dependant libraries
*******/
#include "IMU_AHRS8.h"

AHRS8::AHRS8()
{
	float yaw, pitch, roll;
	float magX, magY, magZ;
	float aclX, aclY, aclZ;
	
	//Attitude variables intialisation
	this->yaw   = 0;
	this->pitch = 0;
	this->roll  = 0;
	
	this->mag[0]  = 0;
	this->mag[1]  = 0;
	this->mag[2]  = 0;
	
	this->acl[0]  = 0;
	this->acl[1]  = 0;
	this->acl[2]  = 0;
	
	this->gyro[0] = 0;
	this->gyro[1] = 0;
	this->gyro[2] = 0;	
	imu_pub = nh.advertise<sensor_msgs::Imu>("imu",2);	
		/* The following is taken from razor imu
	* Orientation covariance estimation:
	* Observed orientation noise: 0.3 degrees in x, y, 0.6 degrees in z
	* Magnetometer linearity: 0.1% of full scale (+/- 2 gauss) => 4 milligauss
	* Earth's magnetic field strength is ~0.5 gauss, so magnetometer nonlinearity could
	* cause ~0.8% yaw error (4mgauss/0.5 gauss = 0.008) => 2.8 degrees, or 0.050 radians
	* i.e. variance in yaw: 0.0025
	* Accelerometer non-linearity: 0.2% of 4G => 0.008G. This could cause
	* static roll/pitch error of 0.8%, owing to gravity orientation sensing
	* error => 2.8 degrees, or 0.05 radians. i.e. variance in roll/pitch: 0.0025
	* so set all covariances the same.
	*/
	imu_msg.orientation_covariance = {
	0.0025 , 0 , 0,
	0, 0.0025, 0,
	0, 0, 0.0025
	};

	/*
	* Angular velocity covariance estimation:
	* Observed gyro noise: 4 counts => 0.28 degrees/sec
	* nonlinearity spec: 0.2% of full scale => 8 degrees/sec = 0.14 rad/sec
	* Choosing the larger (0.14) as std dev, variance = 0.14^2 ~= 0.02
	*/
	imu_msg.angular_velocity_covariance = {
	0.02, 0 , 0,
	0 , 0.02, 0,
	0 , 0 , 0.02
	};

	/*
	* linear acceleration covariance estimation:
	* observed acceleration noise: 5 counts => 20milli-G's ~= 0.2m/s^2
	* nonliniarity spec: 0.5% of full scale => 0.2m/s^2
	* Choosing 0.2 as std dev, variance = 0.2^2 = 0.04
	*/
	imu_msg.linear_acceleration_covariance = {
	0.04 , 0 , 0,
	0 , 0.04, 0,
	0 , 0 , 0.04
	};
}

AHRS8::~AHRS8()
{
	
}

bool AHRS8::serialInitialize(std::string serial_port)
{

	return this->serial.open(serial_port,115200,8,boost::asio::serial_port::parity::none, boost::asio::serial_port_base::stop_bits::one,boost::asio::serial_port::flow_control::none);

}



void AHRS8::loadScripts()
{
	//Script to begin continous fetching of data variables
	std::string IMU_function="forget quatCheck\r : quatCheck begin ?key 0= while magp di. accelp di. gyrop di. cr yaw di. pitch di. roll di. cr 100 delay repeat ;\r ";

	this->serial.IMU_send_command(IMU_function);
	
	//Command to start begin script
	char send_text[]="quatCheck\r";
	this->serial.send(send_text,sizeof(send_text)-1);

}

float AHRS8::getAttitude(std::string attitude)
{
	char *token = std::strtok((char*)attitude.c_str(), " ");
	int i=0;
	    while (token != NULL) 
	    {
		    if(i==2)
		    {
		    	//std::cout<<"Attitude--" << atof(token) << '\n';
		    	return atof(token);
		    }
			
			token = std::strtok(NULL, " ");
			i++;
	    }
}

float AHRS8::getVariants(std::string variant)
{
	char* string_charArray=(char*)variant.c_str();
	char *token = std::strtok((char*)variant.c_str(), " ");
	int i=0, value_position=0;
	
	if(string_charArray[0]!='0')
	{
		value_position=3;
	}
	else
	{
		value_position=1;
	}
	
	while (token != NULL)
	    {
		if(i==value_position)
		{
			//std::cout<<"Variant-- " << atof(token) << '\n';
			return atof(token);
		}
		token = std::strtok(NULL, " ");
		i++;
	    }
   	
   	
   	
}

void AHRS8::parseDataPacket(char* dataPacket)
{
	std::string dataInstr[20];
	int k =0;
	for(int i =0; i< 12; i++){
		while(dataPacket[k] != char(10)){
			dataInstr[i].push_back(dataPacket[k]);
			k++;
		}
		while(dataPacket[k] == char(10) || dataPacket[k] == char(13) || dataPacket[k] == char(0)){
			k++;
		}
	}
	   
	std::string temp;
	//printf("str.......\n");
	
	for(int i=0; i<12; i++)
	{
		char* attribute = (char*)dataInstr[i].c_str();
		//std::cout<<"attribute[0] i--- "<<attribute[0]<<std::endl;
		switch(attribute[0])
		{
			case 'y':
				this->yaw=this->getAttitude(dataInstr[i]);
				break;
			
			case 'p':
				this->pitch=this->getAttitude(dataInstr[i]);
				break;
			
			case 'r':
				this->roll=this->getAttitude(dataInstr[i]);
				break;
			
			case 'm':
				for(int j=0;j<3;j++)
				{
					this->mag[j]=this->getVariants(dataInstr[i]);
					i++;
				}
				i--;
				break;
			
			case 'a':
				for(int j=0;j<3;j++)
				{
					this->acl[j]=this->getVariants(dataInstr[i]);
					i++;
				}
				i--;
				break;
			case 'g':
				for(int j=0;j<3;j++)
				{
					this->gyro[j]=(GYRO_CONSTANT)*(this->getVariants(dataInstr[i]));
					i++;
				}
				i--;
				break;
			
			default:
				break;
			
		}
		

	}
	
}

void AHRS8::fetchSerialData()
{
	char* dataPacket;
	std::string serialResponse;
	
	this->loadScripts();
	uint32_t seq=0;
	while(ros::ok())
	{
		std::string received_text=this->serial.IMU_receive();

		dataPacket=(char*)received_text.c_str();

		this->parseDataPacket(dataPacket);
		/*
		printf("\n\nyaw=%f\npitch=%f\nroll=%f\nmagX=%f\nmagY=%f\nmagZ=%f\naclX=%f\naclY=%f\naclZ=%f\ngyroX=%f\ngyroY=%f\ngyroZ=%f\n",
		this->yaw,this->pitch,this->roll,
		this->mag[0],this->mag[1],this->mag[2],
		this->acl[0],this->acl[1],this->acl[2],
		this->gyro[0],this->gyro[1],this->gyro[2]);*/
		quat = tf::createQuaternionFromRPY(this->roll,this->pitch,this->yaw);
		imu_msg.header.seq = seq++;
		imu_msg.header.stamp = ros::Time::now();
		imu_msg.header.frame_id = "imu";
		imu_msg.orientation.x = quat[0];
		imu_msg.orientation.y = quat[1];
		imu_msg.orientation.z = quat[2];
		imu_msg.orientation.w = quat[3];
		imu_msg.angular_velocity.x = this->gyro[0];
		imu_msg.angular_velocity.y = this->gyro[1];
		imu_msg.angular_velocity.z = this->gyro[2];
		imu_msg.linear_acceleration.x = this->acl[0];
		imu_msg.linear_acceleration.y = this->acl[1];
		imu_msg.linear_acceleration.z = this->acl[2];
		imu_pub.publish(imu_msg);		
	}
	
	
}

float AHRS8::getHeading()
{
	return (this->yaw);
}

float AHRS8::getPitch()
{
	return (this->pitch); 
}

float AHRS8::getRoll()
{
	return (this->roll);
}

