#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>
#include <vector>
int main(int argc, char** argv )
{
	ros::init(argc, argv, "path");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);
	ros::Publisher path_pub = n.advertise<nav_msgs::Path>("path", 1000);
	nav_msgs::Path poses;
	int N_point = 10;
	while(ros::ok())
	{
		int i = 0;
		poses.header.stamp = ros::Time::now();
		poses.poses.resize(10);
		poses.header.stamp = ros::Time::now();
		poses.header.frame_id = "odom";
		while (i < 10) {
			poses.poses[i].header.stamp = ros::Time::now();
			poses.header.frame_id = "odom";
			poses.poses[i].pose.position.x = i;
			poses.poses[i].pose.position.y = i;
			poses.poses[i].pose.orientation.w = i;
			i++;			
		}
		path_pub.publish(poses);
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;

}