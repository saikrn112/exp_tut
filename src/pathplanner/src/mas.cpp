#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
//#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>
#include <vector>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/PolygonStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <cstdlib>
#include <stdio.h>
#include <nav_msgs/Path.h>
using namespace std;


//
static int  xA, yA; float s1,s2; 
static int  xB, yB; float f1,f2;
static int map_1[400][400];                    /*here we are fixing the width and height */
const int m=400;  
const int n=400;
static int closed_nodes_map[n][m];
static int open_nodes_map[n][m]; 
static int dir_map[n][m]; 
const int dir=8; 
static float R;
static int dx[8]={1, 1, 0, -1, -1, -1, 0, 1};
static int dy[8]={0, 1, 1, 1, 0, -1, -1, -1};
//
	nav_msgs::Path msg1;
class node
{
    // current position
    int xPos;
    int yPos;
    // total distance already travelled to reach the node
    int level;
    // priority=level+remaining                    distance estimate
    int priority;  // smaller: higher priority

    public:
        node(int xp, int yp, int d, int p) 
            {xPos=xp; yPos=yp; level=d; priority=p;}
    
        int getxPos() const {return xPos;}
        int getyPos() const {return yPos;}
        int getLevel() const {return level;}
        int getPriority() const {return priority;}

        void updatePriority(const int & xDest, const int & yDest)
        {
             priority=level+estimate(xDest, yDest)*10; //A*
        }

        // give better priority to going strait instead of diagonally
        void nextLevel(const int & i) // i: direction
        {
             level+=(dir==8?(i%2==0?10:14):10);
        }
        
        // Estimation function for the remaining distance to the goal.
        const int & estimate(const int & xDest, const int & yDest) const
        {
            static int xd, yd, d;
            xd=xDest-xPos;
            yd=yDest-yPos;         

            // Euclidian Distance
            d=static_cast<int>(sqrt(xd*xd+yd*yd));

            // Manhattan distance
            //d=abs(xd)+abs(yd);
            
            // Chebyshev distance
            //d=max(abs(xd), abs(yd));

            return(d);
        }
};

// Determine priority (in the priority queue)
bool operator<(const node & a, const node & b)
{
  return a.getPriority() > b.getPriority();
}

// A-star algorithm.
// The route returned is a string of direction digits.
string pathFind( const int & xStart, const int & yStart, 
                 const int & xFinish, const int & yFinish )
{
    static priority_queue<node> pq[2]; // list of open (not-yet-tried) nodes
    static int pqi; // pq index
    static node* n0;
    static node* m0;
    static int i, j, x, y, xdx, ydy;
    static char c;
    pqi=0;
    ROS_INFO ("This is path find ");
    // reset the node maps //Try out better functioin than array 
    for(y=0;y<m;y++)
    { 
        for(x=0;x<n;x++)
        {
            closed_nodes_map[x][y]=0;
            open_nodes_map[x][y]=0; // This can be replaced with list
        }
    }

    // create the start node and push into list of open nodes
    n0=new node(xStart, yStart, 0, 0);
    n0->updatePriority(xFinish, yFinish);
    pq[pqi].push(*n0);
    open_nodes_map[xStart][yStart]=n0->getPriority(); // mark it on the open nodes map

    // A* search
    while(!pq[pqi].empty())
    {
        // get the current node w/ the highest priority
        // from the list of open nodes
        n0=new node( pq[pqi].top().getxPos(), pq[pqi].top().getyPos(), 
                     pq[pqi].top().getLevel(), pq[pqi].top().getPriority());

        x=n0->getxPos(); y=n0->getyPos();
        ROS_INFO ("This is path find 2");
        pq[pqi].pop(); // remove the node from the open list
        open_nodes_map[x][y]=0;
        // mark it on the closed nodes map
        closed_nodes_map[x][y]=1;

        // quit searching when the goal state is reached
        //if((*n0).estimate(xFinish, yFinish) == 0)
        if(x==xFinish && y==yFinish) 
        {
            // generate the path from finish to start
            // by following the directions
            ROS_INFO ("This is path find 3");
            string path="";
            while(!(x==xStart && y==yStart))
            {
                j=dir_map[x][y];
                c='0'+(j+dir/2)%dir;
                path=c+path;
                x+=dx[j];
                y+=dy[j];
                ROS_INFO ("This is path find 4");
            }ROS_INFO ("This is path find 4_1");

            // garbage collection
            delete n0;
            // empty the leftover nodes
            while(!pq[pqi].empty()) pq[pqi].pop();           
            ROS_INFO ("This is path find 4_2");
            return path;
        }
ROS_INFO ("This is path find 5");
        // generate moves (child nodes) in all possible directions
        for(i=0;i<dir;i++)
        {
        	ROS_INFO ("This is path find 6");
            xdx=x+dx[i]; ydy=y+dy[i];

            if(!(xdx<0 || xdx>n-1 || ydy<0 || ydy>m-1 || map_1[xdx][ydy]==100 || closed_nodes_map[xdx][ydy]==1))
            {
            	ROS_INFO ("This is path find 7");
                // generate a child node
                m0=new node( xdx, ydy, n0->getLevel(), 
                             n0->getPriority());
                m0->nextLevel(i);
                m0->updatePriority(xFinish, yFinish);

                // if it is not in the open list then add into that
                if(open_nodes_map[xdx][ydy]==0)
                {
                	ROS_INFO ("This is path find 8");
                    open_nodes_map[xdx][ydy]=m0->getPriority();
                    pq[pqi].push(*m0);
                    // mark its parent node direction
                    dir_map[xdx][ydy]=(i+dir/2)%dir;
                }
                else if(open_nodes_map[xdx][ydy]>m0->getPriority())
                {
                	ROS_INFO ("This is path find 9");
                    // update the priority info
                    open_nodes_map[xdx][ydy]=m0->getPriority();
                    // update the parent direction info
                    dir_map[xdx][ydy]=(i+dir/2)%dir;

                    // replace the node
                    // by emptying one pq to the other one
                    // except the node to be replaced will be ignored
                    // and the new node will be pushed in instead
                    while(!(pq[pqi].top().getxPos()==xdx && 
                           pq[pqi].top().getyPos()==ydy))
                    {                
                    	ROS_INFO ("This is path find 10");
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();       
                    }
                    pq[pqi].pop(); // remove the wanted node
                    
                    // empty the larger size pq to the smaller one
                    if(pq[pqi].size()>pq[1-pqi].size()) pqi=1-pqi;
                    while(!pq[pqi].empty())
                    {                ROS_INFO ("This is path find 11");
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();       
                    }
                    pqi=1-pqi;
                    pq[pqi].push(*m0); // add the better node instead
                }
                else delete m0; // garbage collection
            }
        }
        delete n0; // garbage collection
    }
    return ""; // no route found
}
//
void goalreceived (const geometry_msgs::PoseStamped::ConstPtr& msg)
{

    ROS_INFO_STREAM ("goal position is:"<<msg->pose.position.x <<" "<<msg->pose.position.y<<"\n"
    <<"goal direction is:"<<msg->pose.orientation.x<<msg->pose.orientation.y<<msg->pose.orientation.z<<msg->pose.orientation.w);

    f1 = msg->pose.position.x;
    f2 = msg->pose.position.y;
  	xB=f1/R;
    yB=f2/R;  
      string route=pathFind(xA, yA, xB, yB);
      ROS_INFO("aFter Path find");
      if(route=="") ROS_INFO_STREAM ("An empty route generated!");
       //follow the route on the map and display it 
      if(route.length()>0)
      {
        int j; char c;
        int x=xA;
        int y=yA;
        map_1[x][y]=2;
        msg1.poses.resize(100);
		msg1.header.stamp = ros::Time::now();
		msg1.header.frame_id = "base_link";
		ROS_INFO_STREAM("rl "<<route.length());
        for(int i=0;i<route.length();i++)
        {
        	// ROS_INFO("aFter Path find1 ");

            c =route.at(i);
            j=atoi(&c); 
            x=x+dx[j];
            y=y+dy[j];
            
            // ROS_INFO_STREAM("rl "<<route.length);
            msg1.poses[i].pose.position.x = x*0.05;
		 	msg1.poses[i].pose.position.y = y*0.05;
		 	msg1.poses[i].pose.position.z = 0;
            msg1.poses[i].pose.orientation.w = 0;
            // cout << x;
            ROS_INFO_STREAM("x: "<<msg1.poses[i].pose.position.x << ",y:"<<msg1.poses[i].pose.position.y);
        }
    }
      // string route=pathFind(xA, yA, xB, yB);
      // if(route=="") ROS_INFO_STREAM ("An empty route generated!");
      // //clock_t end = clock();
      //double time_elapsed = double(end - start);
      //ROS_INFO_STRE
}

//
void checkedreceived (const geometry_msgs::PointStamped::ConstPtr& msg)
{
	R = .05;
	ROS_INFO_STREAM ("initial position is:"<<msg->point.x <<","<<msg->point.y<<"\n");
    s1 = msg->point.x;
    s2 = msg->point.y;
    xA=s1/R;
    yA=s2/R;    	
}
//
void mapp (const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
    ROS_INFO_STREAM ("All map data is:\n"<<msg->info.width<<" "<<msg->info.height<<"\n"<<"origin is:"<<msg->info.origin.position.x<<msg->info.origin.position.y<<msg->info.origin.position.z<<"/n orientation is:"<<msg->info.origin.orientation.x<<msg->info.origin.orientation.y<<msg->info.origin.orientation.z<<msg->info.origin.orientation.w);
    std::vector<int8_t>::const_iterator it= msg->data.begin();
    int n=0,m=0;  

    // static int R= msg->info.resolution;
      R = msg->info.resolution;
   
    // for(m=0;m<msg->info.width*msg->info.resolution;m++) 
    // {
    //    for (n=0;n<msg->info.height*msg->info.resolution;n++)
    //    {
    //     map_1[m][n]=*it;  
    //     it++;
    //    }     
    // }
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "code");
	ros::NodeHandle n1;
    srand(time(NULL));
    ros::Rate loop_rate(10);
    ros::Subscriber sub = n1.subscribe("map",5,&mapp);
	 ros::Subscriber sub2 = n1.subscribe("move_base_simple/goal", 1000,&goalreceived);
    ros::Subscriber sub1 = n1.subscribe("clicked_point", 1000, &checkedreceived);
    ros::Publisher path_pub = n1.advertise<nav_msgs::Path>("path", 1000);

	int i=0;
   while(ros::ok())
	{
		
      //clock_t start = clock();
	  // ros::Time::now();
    // AM ("Time to calculate the route (ms): "<<time_elapsed<<endl);
    
    // follow the route on the map and display it 
      
            //map[x][y]=3;
           

        
      
     // ROS_INFO_STREAM ("Map Size (X,Y):" << n << "," << m << "\n" << "Start: " << s1 << "," << s2 << "\n" << "Finish: " << f1 << "," << f2);

            ros::spinOnce();
		    path_pub.publish(msg1);
		    loop_rate.sleep();
    }
    //getchar(); // wait for a (Enter) keypress  
    return(0);

}
//
 // // create empty map
    // for(int y=0;y<m;y++)
    // {
    //     for(int x=0;x<n;x++) map[x][y]=0;
    // }

    // fillout the map matrix with a '+' pattern
    // for(int x=n/8;x<n*7/8;x++)
    // {
    //     map[x][m/2]=1;
    // }
    // for(int y=m/8;y<m*7/8;y++)
    // {
    //     map[n/2][y]=1;
    // }
    
    // randomly select start and finish locations
    // int xA, yA, xB, yB;
    // switch(rand()%8)
    // {
    //     case 0: xA=0;yA=0;xB=n-1;yB=m-1; break;
    //     case 1: xA=0;yA=m-1;xB=n-1;yB=0; break;
    //     case 2: xA=n/2-1;yA=m/2-1;xB=n/2+1;yB=m/2+1; break;
    //     case 3: xA=n/2-1;yA=m/2+1;xB=n/2+1;yB=m/2-1; break;
    //     case 4: xA=n/2-1;yA=0;xB=n/2+1;yB=m-1; break;
    //     case 5: xA=n/2+1;yA=m-1;xB=n/2-1;yB=0; break;
    //     case 6: xA=0;yA=m/2-1;xB=n-1;yB=m/2+1; break;
    //     case 7: xA=n-1;yA=m/2+1;xB=0;yB=m/2-1; break;
    // }

    // cout<<"Map Size (X,Y): "<<n<<","<<m<<endl;
    // cout<<"Start: "<<xA<<","<<yA<<endl;
    // cout<<"Finish: "<<xB<<","<<yB<<endl;
    // // get the route