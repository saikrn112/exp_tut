#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>

void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
ROS_INFO_STREAM( "position" << msg->pose.position.x <<","<< msg->pose.position.y <<","<< msg->pose.position.z  <<"orientation" << msg->pose.orientation.x << ","<<msg->pose.orientation.y << "," << msg->pose.orientation.z << "," << msg->pose.orientation.w );
}

//std::setprecision



void pointCallback(const geometry_msgs::PointStamped::ConstPtr& msg)
{
  ROS_INFO_STREAM("point" << msg->point.x << "," << msg->point.y<< "," << msg->point.z );


}
int main(int argc, char **argv)
{
  ros::init(argc, argv, "goal");
  ros::NodeHandle n;
  ros::Subscriber sub1 = n.subscribe("clicked_point", 1000, &pointCallback);
  ros::Subscriber sub2 = n.subscribe("move_base_simple/goal", 1000, &poseCallback);
 ros::spin();


  return 0;
}

