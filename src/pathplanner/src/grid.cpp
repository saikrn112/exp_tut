#include <ros/ros.h>
#include <std_msgs/String.h>
#include <nav_msgs/OccupancyGrid.h>
#include <vector>
// #include <std_msgs/MultiArrayLayout.h>
// #include <std_msgs/MultiArrayDimension.h>
// #include <std_msgs/Int32MultiArray.h>


void GridFunc(const nav_msgs::OccupancyGrid::ConstPtr& msg)

{
	int i=0;
 int w= msg->info.width;
 int h= msg->info.height;
 int data[w*h];
for(i=0;i<h*w;i++)			
	{
		if(msg->data[i] == -1)		
		data[i] = -1;
		
			else if(msg->data[i] != -1 && msg->data[i] != 0)		
		
	        data[i] = 1;
		
		     else
		    data[i] = 0;
	}
 
   ROS_INFO_STREAM( "resolution" << msg->info.resolution << "\n "<< "width" << msg->info.width << "\n"<<
   	"height" << msg->info.height << "\n"<<"position" << msg->info.origin.position.x << "," << msg->info.origin.position.y << "," << msg->info.origin.position.z
   	<<"\n"<<"orientation" << msg->info.origin.orientation.x<<"," << msg->info.origin.orientation.y<<"," << msg->info.origin.orientation.z
  );
  

  for(i=0;i < w*h-1; i++ ){
  	ROS_INFO_STREAM (""<<data[i]);}



/*


int i = 0;
	
	for(std::vector<int>::const_iterator it = msg->data.begin(); it != msg->data.end(); ++it)
	{
		Arr[i] = *it;
		i++;
	}
*/
return;


}


 int main(int argc, char **argv)
{
	//int j;
  ros::init(argc, argv, "grid");
  ros::NodeHandle n;
  
  
   ros::Subscriber sub1 = n.subscribe("map", 1000, &GridFunc);
  
  ros::spin();

/*  for(j = 1; j < 90; j++)
	{
		printf("%d, ", Arr[j]);
	}

printf("\n");
*/

  return 0;
}