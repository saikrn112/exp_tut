#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>
#include <vector>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PolygonStamped.h>
#include <nav_msgs/OccupancyGrid.h>

#include "igv.h"
using namespace std;
//*****************************************************************************************************************************************
static int  Sx, Sy; float s1,s2; 
static int  Fx, Fy; float f1,f2;
const int m=4000;  //*****************************
const int n=4000; 
static int map_1[n][m];
static int closed_nodes_map[n][m];
static int open_nodes_map[n][m]; 
static int dir_map[n][m]; 
const int dir=8; 
static int R;

static int A[8]={1,1,0,-1,-1,-1,0,1};    //{1,1,-1,-1,1,0,-1,0};
static int B[8]= {0,1,1,1,0,-1,-1,-1}; 




void mapp (const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
    ROS_INFO_STREAM ("All map data is:\n"<<msg->info.width<<" "<<msg->info.height<<"\n"<<"origin is:"<<msg->info.origin.position.x<<msg->info.origin.position.y<<msg->info.origin.position.z<<"/n orientation is:"<<msg->info.origin.orientation.x<<msg->info.origin.orientation.y<<msg->info.origin.orientation.z<<msg->info.origin.orientation.w);
    std::vector<int8_t>::const_iterator it= msg->data.begin();
    int n=0,m=0;  

    static int R= msg->info.resolution;
   
    for(m=0;m<msg->info.width*msg->info.resolution;m++) 
    {
       for (n=0;n<msg->info.height*msg->info.resolution;n++)
       {
        map_1[m][n]=*it;  
        it++;
       }     
    }

    // ROS_INFO("map:"<< map);
}
void goalreceived (const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    ROS_INFO_STREAM ("goal position is:"<<msg->pose.position.x <<" "<<msg->pose.position.y<<"\n"
    <<"goal direction is:"<<msg->pose.orientation.x<<msg->pose.orientation.y<<msg->pose.orientation.z<<msg->pose.orientation.w);

    f1 = msg->pose.position.x;
    f2 = msg->pose.position.y;
  	Fx=f1/R;
 
 	Fy=f2/R;
  
}

void checkedreceived (const geometry_msgs::PointStamped::ConstPtr& msg)
{
	ROS_INFO_STREAM ("initial position is:"<<msg->point.x <<msg->point.y<<"\n");
    s1 = msg->point.x;
    s2 = msg->point.y;
    Sx=s1/R;
    Sy=s2/R;
  
    	
}

int main(int argc, char** argv )
{
	ros::init(argc, argv, "code");
	ros::NodeHandle n;


	// ros::Rate loop_rate(10);
	//ros::Publisher path_pub = n.advertise<geometry_msgs::PoseArray>("path", 1000);
	 ros::Subscriber sub = n.subscribe("map",5,&mapp);
	 ros::Subscriber sub2 = n.subscribe("move_base_simple/goal", 1000,&goalreceived);
     ros::Subscriber sub1 = n.subscribe("clicked_point", 1000, &checkedreceived);
     
     ros::Publisher path_pub = n.advertise<nav_msgs::Path>("path", 1000);
	 nav_msgs::Path msg;
    
     int i = 0;
    srand(time(NULL));
	while(ros::ok())
	{
     clock_t start = clock();
     std::string route=pathFind(Sx, Sy, Fx, Fy);                              ///<--**********have a look*******///
  if(route=="") ROS_INFO_STREAM ("An empty route generated!");
    clock_t end = clock();
    double time_elapsed = double(end - start);
    ROS_INFO_STREAM ("Time to calculate the route (ms): "<<time_elapsed<<endl);
    
   // follow the route on the map and display it 
    if(route.length()>0)
    {
        int j; char c;
        int x=Sx;
        int y=Sy;
        msg.polygon.points.resize(100);
		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = "base_link";
        for(int i=0;i<route.length();i++)
        {
            c =route.at(i);
            j=atoi(&c); 
            x=x+A[j];
            y=y+B[j];
            //map[x][y]=3;
            msg.path.points[i].x = x;
		 	msg.path.points[i].y = y;
		 	msg.path.points[i].z = 1;
        }
        //map[x][y]=4;
    
    }
    ROS_INFO_STREAM ("Map Size (X,Y):"<<n<<","<<m<<"\n"<<"Start: "<<Sx<<","<<Sy<<"\n"<<"Finish: "<<Fx<<","<<Fy);

            ros::spinOnce();
		    path_pub.publish(msg);
		    ros::spinOnce();
		    loop_rate.sleep();
	}
	return 0;

}





/**********************************************************************************************************/
 

// Determine priority (in the priority queue)
bool operator<(const node & a, const node & b)
{
  return a.getf() > b.getf();
}
string pathFind( const int & xS, const int & yS,const int & xF, const int & yF )  //////////////int or string???????????
{
    static priority_queue<node> pq[2]; // list of open (not-yet-tried) nodes
    static int pqi; // pq index
    static node* n0;
    static node* m0;
    static int i, j, x, y, xdx, ydy;
    static char c;

    pqi=0;

    // reset the node maps
    for(y=0;y<m;y++)
    {
        for(x=0;x<n;x++)
        {
            closed_nodes_map[x][y]=0;
            open_nodes_map[x][y]=0;
        }
    }



    // create the start node and push into list of open nodes
    n0 = new node(xS, yS, 0, 0);
    n0->update_f(xF, yF);                                             //*************************************888
    pq[pqi].push(*n0);                                                //***************************************
    open_nodes_map[x][y]=n0->getf(); // mark it on the open nodes map //****************************************8

    // A* search ***************************************************************************************************
    while(!pq[pqi].empty())
    {
        // get the current node w/ the highest priority
        // from the list of open nodes
        n0=new node( pq[pqi].top().getCx(), pq[pqi].top().getCy(), 
                     pq[pqi].top().getg(), pq[pqi].top().getf());

        x=n0->getCx(); y=n0->getCy();

        pq[pqi].pop(); // remove the node from the open list
        open_nodes_map[x][y]=0;
        // mark it on the closed nodes map
        closed_nodes_map[x][y]=1;

        // quit searching when the goal state is reached
        //if((*n0).estimate(xFinish, yFinish) == 0)
        if(x==xF && y==yF) 
        {
            // generate the path from finish to start
            // by following the directions
            string path="";
            while(!(x==xS && y==yS))
            {
                j=dir_map[x][y];
                c='0'+(j+4)%8;  //dir's to 8 substituted
                path=c+path;
                x+=A[j];        //dx and dy as A&B              //////haters gonna hate im gonna rotate
                y+=B[j];
            }

            // garbage collection
            delete n0;
            // empty the leftover nodes
            while(!pq[pqi].empty()) pq[pqi].pop();           
            return path;
        }

        // generate moves (child nodes) in all possible directions
        for(i=0;i<dir;i++)
        {
            xdx=x+A[i]; ydy=y+B[i];
            if(!(xdx<0 || xdx>n-1 || ydy<0 || ydy>m-1 || map_1[xdx][ydy]==100 
                || closed_nodes_map[xdx][ydy]==1))
            {
                // generate a child node
                m0=new node( xdx, ydy, n0->getg(), 
                             n0->getf());
                m0->nextg(i);
                m0->update_f(xF, yF);

                // if it is not in the open list then add into that
                if(open_nodes_map[xdx][ydy]==0)
                {
                    open_nodes_map[xdx][ydy]=m0->getf();
                    pq[pqi].push(*m0);
                    // mark its parent node direction
                    dir_map[xdx][ydy]=(i+4)%8;
                }
                else if(open_nodes_map[xdx][ydy]>m0->getf())
                {
                    // update the priority info
                    open_nodes_map[xdx][ydy]=m0->getf();
                    // update the parent direction info
                    dir_map[xdx][ydy]=(i+dir/2)%dir;

                    // replace the node
                    // by emptying one pq to the other one
                    // except the node to be replaced will be ignored
                    // and the new node will be pushed in instead
                    while(!(pq[pqi].top().getCx()==xdx && 
                           pq[pqi].top().getCy()==ydy))
                    {                
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();       
                    }
                    pq[pqi].pop(); // remove the wanted node
                    
                    // empty the larger size pq to the smaller one
                    if(pq[pqi].size()>pq[1-pqi].size()) pqi=1-pqi;
                    while(!pq[pqi].empty())
                    {                
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();       
                    }
                    pqi=1-pqi;
                    pq[pqi].push(*m0); // add the better node instead
                }
                else delete m0; // garbage collection
            }
        }
        delete n0; // garbage collection
    }
    return ""; // no route found
}

    
   