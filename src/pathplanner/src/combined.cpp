#include <ros/ros.h>
#include <nav_msgs/Path.h>
//#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>


void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
ROS_INFO_STREAM( "position" << msg->pose.position.x <<","<< msg->pose.position.y <<","<< msg->pose.position.z  <<"orientation" << msg->pose.orientation.x << ","<<msg->pose.orientation.y << "," << msg->pose.orientation.z << "," << msg->pose.orientation.w );
}


void pointCallback(const geometry_msgs::PointStamped::ConstPtr& msg)
{
  ROS_INFO_STREAM("point" << msg->point.x << "," << msg->point.y<< "," << msg->point.z );

}

int main(int argc, char** argv )
{
	ros::init(argc, argv, "combined");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);
	ros::Publisher path_pub = n.advertise<geometry_msgs::PolygonStamped>("poly", 1000);
	ros::Subscriber sub1 = n.subscribe("clicked_point", 1000, &pointCallback);
    ros::Subscriber sub2 = n.subscribe("move_base_simple/goal", 1000, &poseCallback);

	// ros::Publisher path_pub = n.advertise<nav_msgs::Path>("path", 1000);

	 //geometry_msgs::Polygon *msg = new geometry_msgs::Polygon[1];
	geometry_msgs::PolygonStamped msg ;
	//std::vector<geometry_msgs::Polygon> ;
	 //geometry_msgs::PoseArray* msg = (geometry_msgs::PoseArray *)malloc(sizeof(std_msgs::Header) + sizeof(geometry_msgs::Pose)*10) ;

	//geometry_msgs::PoseArray* msg = new geometry_msgs::PoseArray[10]; 

	int N_point = 10;
	// msg.poses  = (geometry_msgs::Pose *)malloc(sizeof(geometry_msgs::Pose)*N_point);
	int i = 0;
	while(ros::ok())
	{ 
		i=0;
		msg.polygon.points.resize(100);
		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = "base_link";
		while(i<100)
		{
		 	msg.polygon.points[i].x = i;
		 	msg.polygon.points[i].y = i;
		 	msg.polygon.points[i].z = i;
		 	
		 	i++;
		 }
		    path_pub.publish(msg);
		    ros::spinOnce();
		    loop_rate.sleep();
	}
	return 0;

}
