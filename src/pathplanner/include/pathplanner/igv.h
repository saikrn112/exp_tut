#ifndef IGV_H
#define IGV_H
#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseArray.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>
#include <vector>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PolygonStamped.h>
#include <nav_msgs/OccupancyGrid.h>
// using namespace std;

class node 
{
	int Cx=0; int Cy=0;    //current position
    int g=0, h=0, f=0;     //f=g+h 
    public:node(int xp, int yp, int d, int p) 
            {   Cx=xp; Cy=yp; g=d; f=p;   }

   int getCx() const {return Cx;}
   int getCy() const {return Cy;}
   int getg() const {return g;}
   int getf() const {return f;}

   void update_f(const int & xDest, const int & yDest)
    {
          f=g+heu(xDest, yDest) ;

    }

   const int & heu(const int & xDest, const int & yDest) const
        {
            static int xd,yd,e;
            static float d;
            xd=xDest-Cx;
            yd=yDest-Cy;         

            // Euclidian Distance
            d=static_cast<int>(sqrt(xd*xd+yd*yd));
            d=d*10;
            e=d;

            // Manhattan distance
            //d=abs(xd)+abs(yd);
            
            return(e);
        }
      
};

bool operator<(const node & , const node & );

std::string pathFind( const int & , const int & ,const int & , const int &  );
#endif