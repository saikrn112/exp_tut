import tensorflow as tf 
import numpy as np 

sess = tf.Session()

vector = tf.constant([1.,2.,3.])

print "Diagonal: "
diag = tf.diag(vector)
result = sess.run(diag)
print(result)

# one = tf.constant([[1.,1.,1.],[1.,1.,0],[1.,0,0]])
print "one matrix: "
one = tf.ones(diag.get_shape())
newone = tf.matrix_band_part(one,-1,0)
result = sess.run(newone)
print(result)

print "matsoft: "
matsoft = tf.matmul(newone,diag)
zeros = tf.zeros(diag.get_shape())
equality = tf.not_equal(matsoft,zeros)
result = sess.run(equality)
print(result)

print "softmax: "
soft = tf.nn.softmax(matsoft)
result = sess.run(soft)
print(result)
# result = sess.run(one)
# print(result)