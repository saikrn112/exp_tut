#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
int main(void)
{
  // signal(SIGCHLD, SIG_IGN); /* now I don't have to wait()! */
 pid_t pid;
 int rv;
 int i=0;
 switch(pid = fork()) {
 case -1:
 perror("fork"); /* something went wrong */
 exit(1); /* parent exits */
 case 0:
 printf(" CHILD: This is the child process!\n");
 printf(" CHILD: My PID is %d\n", getpid());
 printf(" CHILD: My parent's PID is %d\n", getppid());
 printf(" CHILD: Enter my exit status (make it small): ");
 // scanf(" %d", &rv);
 printf(" CHILD: I'm outta here!\n");
 for(i=0; i<100; i++) printf("I am child\n");
 exit(rv);
 default:
 printf("PARENT: This is the parent process!\n");
 printf("PARENT: My PID is %d\n", getpid());
 printf("PARENT: My child's PID is %d\n", pid);
 printf("PARENT: I'm now waiting for my child to exit()...\n");
 // wait(&rv);
 int i=0;
 for(i=0; i<100; i++) printf("I am parent\n");
 printf("PARENT: My child's exit status is: %d\n", WEXITSTATUS(rv));
 printf("PARENT: I'm outta here!\n");
 }
 // int i=0;
 // for(i=0; i<100; i++) printf("I am child\n");
 return 0;
}
