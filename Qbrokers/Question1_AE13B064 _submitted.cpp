#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


ofstream streamOutput1, streamOutput2;
ofstream streamStats1, streamStats2;

class udpParser{
  int expectedSequenceNumber_;
  int missed_;
  int maxMisses_;
  int currSeq_;
public:
  udpParser();
  void takedata(int , int, int);
  void process(int, int, int);
  void inOrder(); // checks if this in order
  bool thisIsExpected(); // Checks if this expected
  int nPacketsLost_;
  int nOutOfOrder_;
  int nDiscarded_;
  int seq_;
  int time_;
  int channel_;

  ~udpParser();
};

udpParser::udpParser():
maxMisses_(5),
nPacketsLost_(0),
nOutOfOrder_(0),
nDiscarded_(0),
missed_(0)
{

}

void udpParser::takedata( int channel,int startSeq_i, int time)

{
  channel_ = channel;
  seq_ = startSeq_i;
  time_ =time;

  currSeq_ = startSeq_i;
  expectedSequenceNumber_ = startSeq_i + 1;
  // need to write to publishToCSVs
}

udpParser::~udpParser(){
}

void publishSequence(int channel , int seq, int time){
  string s; s = to_string(channel) + "," + to_string(seq) + "," + to_string(time);
  if (channel == 1) streamOutput1<< s;
  if (channel == 2) streamOutput2<< s;
}

void publishStats(int channel, int nPacketsLost, int nOutOfOrder, int nDiscarded){
  string s;
  s = to_string(nPacketsLost) + "," + to_string(nOutOfOrder) + ","+ to_string(nDiscarded);
  if(channel == 1)streamStats1 << s;
  if(channel == 2)streamStats2 << s;
}

void udpParser::process(int channel, int seq, int time){
  channel_ = channel;
  seq_ = seq;
  time_ = time;
  if(currSeq_ < seq_ ) currSeq_ = seq_;
  if(thisIsExpected()){
    publishSequence(channel, seq, time);
  }
}

void udpParser::inOrder(){
  if(seq_ < currSeq_){
    nOutOfOrder_++;
  }
}

bool udpParser::thisIsExpected(){
  if(seq_ < expectedSequenceNumber_ ){
    nDiscarded_++;
    return false;
  } else if( seq_ > expectedSequenceNumber_ ){
    if(missed_ > maxMisses_ ){
      missed_ = 0;
      expectedSequenceNumber_++;
      nPacketsLost_++;
      return false;
    } else {
      inOrder();
      missed_++;
    }
  }
  return true;
}

int main(){
  /* Initialising the variable */
	int seq=0,time=0, channel=0;
	string line,time_s, channel_s,seq_s;
	ifstream myfile;
	vector<bool> isChannelInit(3, false);
  ofstream streamOutput1, streamOutput2;
  ofstream streamStats1, streamStats2;
	/* File Operations */
	myfile.open("UDP Packets - Sheet2.csv"); // opening the file
  streamOutput1.open("output_AE13B064_1.csv");
  streamOutput2.open("output_AE13B064_2.csv");
  streamStats1.open("statistic_AE13B064_1.csv");
  streamStats2.open("statistic_AE13B064_2.csv");
	if (myfile.is_open()) {
		getline(myfile,line);  // Discard this line
	} else {
		cout << "file UDP couldn't open" << endl;
		return -1;
	}
  if (streamOutput1.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file output 1 couldn't open" << endl;
		return -1;
	}

  if (streamOutput2.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file output 2 couldn't open" << endl;
		return -1;
	}
  if (streamStats1.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file stats 1 couldn't open" << endl;
		return -1;
	}

  if (streamStats2.is_open()) {
    //getline(myfile,line);  // Discard this line
  } else {
    cout << "file stats 2 couldn't open" << endl;
    return -1;
  }


  udpParser uParser1, uParser2;

	/* Parsing the file and storing in the hashmap */
	while(getline(myfile,channel_s,',')){
		// getline(myfile,line,',');
    channel = stoi(channel_s);
		getline(myfile,seq_s,',');  time = stoi(time_s);
    getline(myfile,time_s); seq = stoi(seq_s);
		if(isChannelInit[channel]){
      if(channel == 1) uParser1.process(channel, seq, time);
      if(channel == 2) uParser2.process(channel, seq, time);
    } else if(channel == 1){
      uParser1.takedata(channel, seq, time);
    } else if(channel == 2){
      uParser2.takedata(channel, seq, time);
    }
	}
	myfile.close();
  publishStats( 1, uParser1.nPacketsLost_, uParser1.nOutOfOrder_, uParser1.nDiscarded_);
  publishStats(2, uParser2.nPacketsLost_, uParser2.nOutOfOrder_, uParser2.nDiscarded_);
  return 1;

}
