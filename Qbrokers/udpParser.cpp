#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

class udpParser{
  int seq_;
  string time_;
  int channel_;
  int nPacketsLost_;
  int nOutOfOrder_;
  int nDiscarded_;
  int expectedSequenceNumber_;
  int missed_;
  int maxMisses_;
  int currSeq_;
  ofstream *streamOutput_;
  ofstream *streamStats_;
public:
  udpParser();
  udpParser(int , int, string, ofstream *, ofstream*);
  void take(int, int, int);
  void process(int, int, string);
  void inOrder(); // checks if this in order
  bool thisIsExpected(); // Checks if this expected
  void publishSequence(); // publishes the channel data;
  void publishStats(); // publishes stats of this particular channel in CSV;
  ~udpParser();
};

udpParser::udpParser( ){

}

udpParser::udpParser( int channel,int startSeq_i, string time, ofstream *stroutput, ofstream *strstats ):
 channel_(channel),
 seq_(startSeq_i),
 time_(time),
 maxMisses_(5),
 nPacketsLost_(0),
 nOutOfOrder_(0),
 nDiscarded_(0),
 missed_(0),
 currSeq_(startSeq_i),
 streamOutput_( stroutput),
 streamStats_(strstats)
{
  expectedSequenceNumber_ = seq_ + 1;
  // cout << seq_ << endl;
  // need to write to publishToCSVs
}

udpParser::~udpParser(){
  // streamOutput_->close();
  // streamStats_->close();
}

void udpParser::publishSequence(){
  string s; s = std::to_string(channel_) + "," + std::to_string(seq_) + "," + time_;
  *streamOutput_<< s;
}

void udpParser::publishStats(){
  string s; s = std::to_string(nPacketsLost_) + "," + std::to_string(nOutOfOrder_) + "," + std::to_string(nDiscarded_);
  cout << s << endl;
  *streamStats_ << s;
}

void udpParser::process(int channel, int seq, string time){
  channel_ = channel;
  seq_ = seq;
  time_ = time;
  // cout << channel_ << " " << seq_ << " " << time_ << endl;
  if(currSeq_ < seq_ ) currSeq_ = seq_;
  if(thisIsExpected()){
    publishSequence();
  }
}

void udpParser::inOrder(){
  if(seq_ < currSeq_){
    nOutOfOrder_++;
  }
}

bool udpParser::thisIsExpected(){
  //cout << expectedSequenceNumber_ << endl;
  if(seq_ < expectedSequenceNumber_ ){
    nDiscarded_++;
    return false;
  } else if( seq_ > expectedSequenceNumber_ ){
    if(missed_ > maxMisses_ ){
      missed_ = 0;
      expectedSequenceNumber_++;
      nPacketsLost_++;
      inOrder();
    } else {
      inOrder();
      missed_++;
    }
  }
  else if(seq_ == expectedSequenceNumber_){
    expectedSequenceNumber_++;
  }
  return true;
}

int main(){
  /* Initialising the variable */
	int seq=0, channel=0;
  string time;
	string line,time_s, channel_s,seq_s;
	ifstream myfile;
	vector<bool> isChannelInit(2, false);
  udpParser uParser1, uParser2;
  ofstream streamOutput1, streamOutput2;
  ofstream streamStats1, streamStats2;
	/* File Operations */
	myfile.open("UDP Packets - Sheet2.csv"); // opening the file
  streamOutput1.open("output_AE13B064_1.csv");
  streamOutput2.open("output_AE13B064_2.csv");
  streamStats1.open("statistic_AE13B064_1.csv");
  streamStats2.open("statistic_AE13B064_2.csv");
	if (myfile.is_open()) {
		getline(myfile,line);  // Discard this line
    // cout << line << endl;
	} else {
		cout << "file UDP couldn't open" << endl;
		return -1;
	}
  if (streamOutput1.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file output 1 couldn't open" << endl;
		return -1;
	}

  if (streamOutput2.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file output 2 couldn't open" << endl;
		return -1;
	}
  if (streamStats1.is_open()) {
		//getline(myfile,line);  // Discard this line
	} else {
		cout << "file stats 1 couldn't open" << endl;
		return -1;
	}

  if (streamStats2.is_open()) {
    //getline(myfile,line);  // Discard this line
  } else {
    cout << "file stats 2 couldn't open" << endl;
    return -1;
  }

  // getline(myfile,channel_s,',');
  // cout << channel_s << endl;
	/* Parsing the file and storing in the hashmap */
	while(getline(myfile,channel_s,',')){
		// getline(myfile,line,',');
    channel = stoi(channel_s); //cout << "c" << channel << endl;
		getline(myfile,seq_s,','); seq = stoi(seq_s); //cout << "s"<< seq << endl;
    getline(myfile,time_s); time = time_s; //cout <<"t"<< time << endl;
    // cout << channel << " " << time << " " << seq << endl;
		if(isChannelInit[channel-1]){
      if(channel == 1) uParser1.process(channel, seq, time);
      if(channel == 2) uParser2.process(channel, seq, time);
    } else if(channel == 1){
      // cout <<"dd"<< channel << " " << seq << " " << time << endl;
      uParser1 = udpParser(channel, seq, time, &streamOutput1, &streamStats1);
      isChannelInit[0] = true;
    } else if(channel == 2){
      // cout <<"dd1"<< channel << " " << seq << " " << time << endl;
      uParser2 = udpParser(channel, seq, time, &streamOutput2, &streamStats2);
      isChannelInit[1] = true;
    }
	}
	myfile.close();
  uParser1.publishStats();
  uParser2.publishStats();

  streamOutput1.close(); streamOutput2.close();
  streamStats1.close(); streamStats2.close();
  // delete uParser1;
  // delete uParser2;
  return 1;

}
