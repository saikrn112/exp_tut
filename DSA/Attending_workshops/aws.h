#ifndef AWS
#define AWS
// #include <bits/stdc++.h> //Includes every Standard library and Standard Template Library and their header files
#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream> // This set of include files reduced the binary file from 50kB to 47.5kB

namespace workshop{
	struct Workshop{
	    int start_time; 
	    int duration;
	    int end_time;
	};
	struct Available_Workshops{
	    int n;
	    std::vector<Workshop> workshop;
	};
	bool mycomp(Workshop ,Workshop );
	Available_Workshops* initialize(int* ,int* ,int );
	int CalculateMaxWorkshops(Available_Workshops* );
}
#endif