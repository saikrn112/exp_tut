/* Available workshops is from C++ Other concepts section Hackerrank */
/* This code is an example of basic version of Greedy algorithms */

#include "aws.h"
using namespace std;

int main(){
    int n; // number of workshops
    int* start_time = new int[n];
    int* duration = new int[n];
    string line;
    ifstream myfile;

   	/* File Operations */
	myfile.open("myfile.txt"); // opening the file
	if (myfile.is_open()) {
		getline(myfile,line); n = stoi(line); // First element of the file : Number of workshops
	} else {
		cout << "file couldn't open" << endl;
		return 1;
	}

	/* Parsing the file and storing in the array */
    for(int i=0; i < n; i++) myfile >> start_time[i];
    for(int i=0; i < n; i++) myfile >> duration[i];
	   myfile.close();

    workshop::Available_Workshops * ptr;
    ptr = workshop::initialize(start_time,duration, n);
    cout << workshop::CalculateMaxWorkshops(ptr) << endl;
	return 0;
}
