#include "aws.h"

namespace workshop {
	/* This is the comparision code for the workshop structure */
	bool mycomp(Workshop a ,Workshop b ) {
	    return a.end_time < b.end_time;
	}

	/* This part of the code takes initial and duration arrays and sorted according to their end time*/
	Available_Workshops* initialize(int start_time[],int duration[],int n){
	    Available_Workshops* aws = new Available_Workshops;
	    std::vector<Workshop> ws; ws.resize(n);

	    /* Storing values inside vectorized workshop structure */
        for(int i=0;i<n;i++){
	        ws[i].start_time = start_time[i];
	        ws[i].duration = duration[i];
	        ws[i].end_time = start_time[i] + duration[i];
	    }

	    std::sort(ws.begin(),ws.end(),mycomp); // Sorting the array 

	    /* Loops for printing elements */
	    for(int i=0;i<n;i++) std::cout<< ws[i].start_time << " "; std::cout << std::endl;
	    for(int i=0;i<n;i++) std::cout<< ws[i].duration << " "; std::cout << std::endl;
	    for(int i=0;i<n;i++) std::cout<< ws[i].end_time << " "; std::cout << std::endl;

	    /* Returning Available workshop structure pointer*/
	    aws->n = n;
	    aws->workshop = ws;
	    return aws;
	}

	/* Greedy Algorithm for calculating available workshops */
	int CalculateMaxWorkshops(Available_Workshops* ptr){
		int counter = 1;
		int j = 0;

		for(int i=1; i < ptr->n; i++){
			if(ptr->workshop[i].start_time >= ptr->workshop[j].end_time){
				counter++;
				j = i;
			}
		}

	    return counter;
	}
}
