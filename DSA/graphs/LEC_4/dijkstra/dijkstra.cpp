#include <iostream>
#include <vector>
#include <queue>
#include <fstream>
#include <climits>

using std::vector;
using std::queue;
using std::pair;
using std::priority_queue;
using namespace std;

void printList(vector<vector<int>> &adj){
  for(int i=0; i<adj.size(); i++){
    cout << i << " :";
    for(auto j:adj[i]){
      cout << j << " ";
    }
    cout << endl;
  }
}

class comparator{
  public:
    bool operator()(const pair<int,int> i,const pair<int,int> j){
      return i.second > j.second;
    }
};

int distance(vector<vector<int> > &adj, vector<vector<int> > &cost, int s, int t) {
  //write your code here
  int counter = 0;
  vector<int> dist(adj.size(),INT_MAX);
  priority_queue< pair<int,int>, vector<pair<int,int>>, comparator> nodeDistances;
  dist[s] = 0;

  pair<int,int> cellDistance;
  cellDistance.first = s; // Index
  cellDistance.second = dist[s]; // Cost to reach this index from s
  nodeDistances.push(cellDistance);

  while(!nodeDistances.empty() ){
    cellDistance = nodeDistances.top();
    nodeDistances.pop();
    int idx = cellDistance.first;
    int cellCost = cellDistance.second;
    for(int i=0; i<adj[idx].size(); i++){
      if(dist[adj[idx][i]] > cellCost + cost[idx][i]){
        dist[adj[idx][i]] = cellCost + cost[idx][i];
        pair<int,int> ele;
        ele.first = adj[idx][i];
        ele.second = dist[adj[idx][i]];
        nodeDistances.push(ele);
        counter++;
      }
    }
  }
  if(dist[t]== adj.size()) return -1;
  return dist[t];
}

int main() {
  int n, m;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n >> m;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }


  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  vector<vector<int> > cost(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y, w;
    // std::cin >> x >> y >> w;
    myfile >> x >> y >> w;
    adj[x - 1].push_back(y - 1);
    cost[x - 1].push_back(w);
  }
  // printList(adj);
  int s, t;
  // std::cin >> s >> t;
  myfile >> s >> t;
  s--, t--;
  // cout << s << t << endl;;
  std::cout << distance(adj, cost, s, t) << endl;
}
