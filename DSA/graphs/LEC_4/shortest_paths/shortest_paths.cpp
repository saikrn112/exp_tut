#include <iostream>
#include <limits>
#include <vector>
#include <queue>
#include <fstream>

using std::vector;
using std::queue;
using std::pair;
using std::priority_queue;
using std::cout;
using std::endl;
using std::string;
using std::ifstream;

bool relax(int i, int j, vector<vector<int>> &adj, vector<vector<int>> &cost, vector<long long> &distance, vector<int> &reachable) {
  if(distance[i] == std::numeric_limits<long long>::max()){
    reachable[i] = false;
    return false;
  }
  if(distance[adj[i][j]] > distance[i] + cost[i][j]){
    distance[adj[i][j]] = distance[i] + cost[i][j];
    reachable[i] = true;
    return true;
  }
  return false;
}

void bfs(int i, vector<vector<int>> &adj, queue<int> &q, vector<int> &visited){
  if(visited[i]==true) return;
  visited[i] = true;
  q.push(i);
  for(int j=0; j<adj[i].size(); j++){
    bfs(adj[i][j], adj, q, visited);
  }
}

void shortest_paths(vector<vector<int> > &adj, vector<vector<int> > &cost, int s, vector<long long> &distance, vector<int> &reachable, vector<int> &shortest) {
  //write your code here
  distance[s] = 0;
  for(int i=0; i<adj.size(); i++){
    for(int j=0; j<adj[i].size(); j++){
      relax(i, j, adj, cost, distance, reachable);
    }
  }
  // for(auto i:distance) cout << i << " ";
  // cout << endl;
  // for(auto i:reachable) cout << i << " ";
  // cout << endl;
  // for(auto i:shortest) cout << i << " ";
  // cout << endl;

  queue<int> q;
  vector<int> visited(adj.size(), 0);
  for(int i=0; i<adj.size(); i++){
    if(!visited[i]){
      for(int j=0; j<adj[i].size(); j++){
        if(relax(i, j, adj, cost, distance, reachable)){
          bfs(adj[i][j],adj, q, visited);
        }
      }
    }
  }

  while(!q.empty()){
    int i = q.front();
    shortest[i] = false;
    q.pop();
  }

}

int main() {
  int n, m, s;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n >> m;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }

  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  vector<vector<int> > cost(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y, w;
    // std::cin >> x >> y >> w;
    myfile >> x >> y >> w;
    adj[x - 1].push_back(y - 1);
    cost[x - 1].push_back(w);
  }
  // std::cin >> s;
  myfile >> s;
  s--;
  vector<long long> distance(n, std::numeric_limits<long long>::max());
  vector<int> reachable(n, 0);
  vector<int> shortest(n, 1);
  shortest_paths(adj, cost, s, distance, reachable, shortest);
  for (int i = 0; i < n; i++) {
    if (!reachable[i]) {
      std::cout << "*\n";
    } else if (!shortest[i]) {
      std::cout << "-\n";
    } else {
      std::cout << distance[i] << "\n";
    }
  }
}
