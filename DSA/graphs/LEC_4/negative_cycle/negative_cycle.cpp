#include <iostream>
#include <vector>
#include <climits>
#include <fstream>

using namespace std;
using std::vector;

bool relax(int i, int j, vector<vector<int>> &adj, vector<vector<int>> &cost, vector<int> &dist){
  if(dist[i]==INT_MAX) return false;
  if(dist[adj[i][j]] > dist[i] + cost[i][j]){
    dist[adj[i][j]] = dist[i] + cost[i][j];
    return true;
  }
  return false;
}

int negative_cycle(vector<vector<int> > &adj, vector<vector<int> > &cost) {
  //write your code here
  vector<int> dist(adj.size(), INT_MAX);
  dist[0] = 0;
  for(int i=0; i<adj.size(); i++){
    for(int j=0; j<adj[i].size(); j++){
      relax(i,j,adj,cost,dist);
      // cout << i << " "<< dist[i] << " "<< adj[i][j]  << " " << dist[adj[i][j]] << endl;
    }
  }
  
  for(int i=0; i<adj.size(); i++){
    for(int j=0; j<adj[i].size(); j++){
      if(relax(i,j,adj,cost,dist)==true) return 1;
    }
  }
  return 0;
}

int main() {
  int n, m;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n >> m;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }

  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  vector<vector<int> > cost(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y, w;
    // std::cin >> x >> y >> w;
    myfile >> x >> y >> w;
    adj[x - 1].push_back(y - 1);
    cost[x - 1].push_back(w);
  }
  std::cout << negative_cycle(adj, cost) << endl;
}
