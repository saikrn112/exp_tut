#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <vector>
#include <cmath>
#include <fstream>

using std::vector;
using std::pair;
using namespace std;


struct point{
public:
  int x;
  int y;
  int id;
  point(int x, int y, int i):x(x),y(y), id(i){};
} typedef point;


double distance(point a, point b){
  return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}


struct Edge{
public:
  point pointA;
  point pointB;
  double dist;
  Edge(point a, point b):pointA(a), pointB(b){
    dist = distance(pointA,pointB);
  }
} typedef Edge;

bool comparator(Edge a, Edge b){
  return a.dist < b.dist;
}


int find(int i, vector<int> &parent){
  if(i!= parent[i]){
    parent[i] = find(parent[i], parent); // path compression heuristic
  }
  return parent[i];
}

void mergeSet(int i, int j, vector<int> &parent, vector<int> &rank){
  int i_id = find(i, parent);
  int j_id = find(j, parent);
  if(i_id == j_id) return;
  if(rank[i_id]<= rank[j_id]){
    parent[i_id] = j_id;
    if(rank[i_id] == rank[j_id])rank[j_id]++;
  } else if(rank[i_id]>rank[j_id] ){
    parent[j_id] = i_id;
  }
  return;
}


double clustering(vector<int> x, vector<int> y, int k) {
  //write your code here

  if(x.size()==k) return 0;
  int counter = x.size();
  vector<int> parent(x.size(),0);
  // making parent
  iota(begin(parent),end(parent),0);
  // making rank
  vector<int> rank(x.size(),0);

  // creating edges
  vector<Edge> edges;
  for (int i=0; i<x.size(); i++){
    point a(x[i],y[i],i);
    for(int j=i+1; j<x.size(); j++){
      point b(x[j],y[j],j);
      Edge A(a,b);
      edges.push_back(A);
    }
  }

  // sorting edges
  sort(edges.begin(),edges.end(), comparator);
  // for(int i=0; i<edges.size(); i++){
  //   cout<<i << " " << edges[i].pointA.id << "(" << edges[i].pointA.x <<"," <<edges[i].pointA.y << ") and"<<edges[i].pointB.id<<" (" << edges[i].pointB.x << "," <<edges[i].pointB.y ;
  //   cout << ") dist: " << edges[i].dist << endl;
  // }

  double dist = 0;
  for(int i=0; i<edges.size(); i++){
    // checking for tree condition
    // cout << i << " " << counter << " " << edges[i].dist << endl;
    if(counter== k){
      if(find(edges[i].pointA.id, parent) != find(edges[i].pointB.id, parent)){
        return edges[i].dist;

      }
      // i++;
      continue;
    }
    // if they are not in same set include them
    if(find(edges[i].pointA.id, parent) != find(edges[i].pointB.id, parent)){
      // union of sets
      mergeSet(edges[i].pointA.id,edges[i].pointB.id, parent, rank);
      // dist += edges[i].dist;
      counter--;
    }
  }
  return dist;
  // return -1;
}

int main() {
  size_t n;
  int k;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n ;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }

  // std::cin >> n;
  vector<int> x(n), y(n);
  for (size_t i = 0; i < n; i++) {
    // std::cin >> x[i] >> y[i];
    myfile >> x[i] >> y[i];
  }
  // std::cin >> k;
  myfile >> k;
  std::cout << std::setprecision(10) << clustering(x, y, k) << std::endl;
}
