#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;

using std::vector;
using std::pair;

void dfs(vector<vector<int> > &adj, vector<int> &used, vector<int> &order, int x) {
  //write your code here
  if(used[x]==true) return;
  used[x] = true;
  for(int i=0; i<adj[x].size(); i++){
    dfs(adj,used,order,i);
  }
  order.push_back(x);

}

vector<int> toposort(vector<vector<int> > adj) {
  vector<int> used(adj.size(), 0);
  vector<int> order;
  //write your code here
  for(int i=0; i<adj.size(); i++){
    if(used[i]!=true){
      dfs(adj, used, order, i);
      // order.push_back(i);
    }
  }
  reverse(order.begin(),order.end());
  return order;
}

void printList(vector<vector<int>> &adj){
  for(int i=0; i<adj.size(); i++){
    cout << i << " :";
    for(auto j:adj[i]){
      cout << j << " ";
    }
    cout << endl;
  }
}


int main() {
  size_t n, m;
  string line;
  ifstream myfile;

   /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
    //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
    myfile >> n >> m;
    cout << n <<" " <<  m << endl;;
  } else {
    std::cout << "file couldn't open" << std::endl;
    return 1;
  }


  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (size_t i = 0; i < m; i++) {
    int x, y;
    myfile >> x >> y;
    cout << x <<" " <<  y << endl;;
    adj[x - 1].push_back(y - 1);
  }
  printList(adj);
  vector<int> order = toposort(adj);
  for (size_t i = 0; i < order.size(); i++) {
    std::cout << order[i] + 1 << " ";
  }
}
