#include <algorithm>
#include <iostream>
#include <vector>
#include <fstream>

using std::vector;
using std::pair;
using namespace std;

void printList(vector<vector<int>> &adj){
  for(int i=0; i<adj.size(); i++){
    cout << i << " :";
    for(auto j:adj[i]){
      cout << j << " ";
    }
    cout << endl;
  }
}

void printOrder(vector<pair<int,int>> &postOrder){
  cout <<"postOrder" <<endl;
  for(int i=0; i<postOrder.size(); i++){
    cout << postOrder[i].first <<" "<< postOrder[i].second << endl;
  }
  cout << endl;
}

vector<vector<int>> reverse(vector<vector<int>> &adj1){
  vector<vector<int>> adj2(adj1.size());
  for(int i=0; i<adj1.size(); i++){
    for(int j=0; j<adj1[i].size(); j++){
      adj2[adj1[i][j]].push_back(i);
    }
  }
  // cout << endl;
  // printList(adj2);
  return adj2;
}

bool comparator(pair<int,int> i, pair<int,int> j){
  return i.second > j.second;
}

void DFS1(vector<vector<int>> &adj, vector<int> &visited, vector<pair<int,int>> &postOrder, int i, int &counter){
  if(visited[i]== true) return;
  visited[i] = true;
  for(int j=0; j< adj[i].size(); j++){
    DFS1(adj, visited, postOrder, adj[i][j], counter);
  }
  postOrder[i].second = counter++;
  return;
}

void DFS2(vector<vector<int>> &adj, vector<int> &selected, vector<int> &subSet, int i){
  if(selected[i]== true) return;
  selected[i] = true;
  subSet.push_back(i);
  for(int j=0; j<adj[i].size(); j++){
    DFS2(adj, selected, subSet, adj[i][j]);
  }
  return;
}

int number_of_strongly_connected_components(vector<vector<int> > adj) {
  //write your code here
  int result = 0;
  // reversing the graph
  vector<vector<int>> adj2 = reverse(adj);

  // creating a postOrder list
  vector<pair<int,int>> postOrder(adj.size());
  for(int i=0; i<adj.size(); i++){
    postOrder[i].first = i;
    postOrder[i].second = 0;
  }

  // creating a visited and counter list
  vector<int> visited(adj.size(),0);
  int counter =0;

  // finding postOrders of nodes
  for(int i=0; i<adj2.size(); i++){
    if(visited[i]!=true){
      DFS1(adj2, visited, postOrder, i, counter);
      // postOrder[i].second = counter++;
    }
  }

  // printOrder(postOrder);

  // reverse sort of postOrder using comparator
  sort(postOrder.begin(),postOrder.end(), comparator);

  // creating a selected and set list
  vector<int> selected(adj.size(), 0);
  vector<vector<int>> powerSet;

  // finding the strongly connected components in the main graph
  for(int i=0; i<adj.size(); i++){
    vector<int> subSet;
    if(selected[i]!= true){
      DFS2(adj, selected, subSet, i);
      powerSet.push_back(subSet);
    }
  }
  // printList(powerSet);
  return powerSet.size();
  // return 0;

}

int main() {
  size_t n, m;
  string line;
  ifstream myfile;

   /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
    //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
    myfile >> n >> m;
  } else {
    std::cout << "file couldn't open" << std::endl;
    return 1;
  }


  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (size_t i = 0; i < m; i++) {
    int x, y;
    // std::cin >> x >> y;
    myfile >> x >> y;
    adj[x - 1].push_back(y - 1);
  }
  // printList(adj);
  std::cout << number_of_strongly_connected_components(adj) << endl;
}
