#include <iostream>
#include <vector>
#include <fstream>

using namespace std;
using std::vector;
using std::pair;

int DFS(vector<vector<int>> &adj, vector<bool> &visited , vector<bool> &currIter, int i){
  // cout <<endl<< i <<  " " << visited[i] <<" " <<  currIter[i] << endl;
  if(visited[i]==true and currIter[i]==true) return 1;
  if(visited[i]==true and currIter[i]==false) return 0;
  if(visited[i]!=true) visited[i] = true;
  if(currIter[i]!=true) currIter[i] = true;

  int k = 0;

  for(int j=0; j<adj[i].size(); j++){
    k = DFS(adj, visited, currIter, adj[i][j]);
    // cout <<i<< " "<< adj[i][j] <<" "<< k <<endl;
    if(k==1){
      return 1;
    }

  }
  currIter[i] = false ;
  return 0;
}

void printList(vector<vector<int>> &adj){
  for(int i=0; i<adj.size(); i++){
    cout << i << " :";
    for(auto j:adj[i]){
      cout << j << " ";
    }
    cout << endl;
  }
}

int acyclic(vector<vector<int>> &adj) {
  vector<bool> visited(adj.size(),false);
  vector<bool> currIter(adj.size(),false);
  int k = 0;
  for(int i=0; i<adj.size(); i++){
    if(visited[i]!=true){
      k = DFS(adj, visited, currIter, i);
    }
    if(k==1) return 1;
  }
  return 0;
}

int main() {
  size_t n, m;

  string line;
  ifstream myfile;

   /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
    //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
    myfile >> n >> m;
    // cout << n <<" " <<  m << endl;;
  } else {
    std::cout << "file couldn't open" << std::endl;
    return 1;
  }

  //myfile >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (size_t i = 0; i < m; i++) {
    int x, y;
    myfile >> x >> y;
    // cout << x <<" " <<  y << endl;;
    adj[x - 1].push_back(y - 1);
  }
  // printList(adj);
  std::cout << acyclic(adj) << endl;;
}
