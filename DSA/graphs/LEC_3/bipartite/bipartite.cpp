#include <iostream>
#include <vector>
#include <queue>
#include <fstream>

using std::vector;
using std::queue;
using namespace std;

int bipartite(vector<vector<int> > &adj) {
  //write your code here
  queue<int> q;
  vector<int> dist(adj.size(),adj.size());
  vector<int> red(adj.size(),0);
  vector<int> white(adj.size(),0);
  q.push(0);
  red[0] = 1;
  dist[0] = 0;
  while(!q.empty()){
    int i = q.front();
    for(int j=0; j<adj[i].size(); j++){
      if(dist[i]%2 == 0){
        if(red[adj[i][j]]== 1)
          return 0;
        white[adj[i][j]] = 1;
      }
      if(dist[i]%2 == 1){
        if(white[adj[i][j]]== 1)
          return 0;
        red[adj[i][j]] = 1;
      }
      if(dist[adj[i][j]] == adj.size()){
        q.push(adj[i][j]);
        dist[adj[i][j]] = dist[i] + 1;
      }
      // cout << i << " " << adj[i][j] << " " << dist[i] << endl;
    }
    q.pop();
  }
  return 1;
}

int main() {
  int n, m;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n >> m;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }

  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y;
    // std::cin >> x >> y;
    myfile >> x >> y;
    adj[x - 1].push_back(y - 1);
    adj[y - 1].push_back(x - 1);
  }
  std::cout << bipartite(adj);
}
