#include <iostream>
#include <vector>
#include <queue>
#include <fstream>

using std::vector;
using std::queue;
using namespace std;

int distance(vector<vector<int> > &adj, int s, int t) {
  //write your code here
  if(s==t) return 0;
  queue<int> q; q.push(s);
  vector<int> dist(adj.size(),adj.size());
  dist[s] = 0;
  while(!q.empty()){
    int i = q.front();
    for(int j=0; j<adj[i].size(); j++){
      if(dist[adj[i][j]]==adj.size()){
        q.push(adj[i][j]);
        dist[adj[i][j]] = dist[i] + 1;
        if(adj[i][j] == t) return dist[adj[i][j]];
      }
    }
    q.pop();
  }
  return -1;
}

int main() {
  int n, m;
  string line;
  ifstream myfile;

  /* File Operations */
  myfile.open("myfile.txt"); // opening the file
  if (myfile.is_open()) {
   //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
   myfile >> n >> m;
  } else {
   std::cout << "file couldn't open" << std::endl;
   return 1;
  }


  // std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (int i = 0; i < m; i++) {
    int x, y;
    myfile >> x >> y;
    // std::cout >> x >> y;
    adj[x - 1].push_back(y - 1);
    adj[y - 1].push_back(x - 1);
  }
  int s, t;
  // std::cin >> s >> t;
  myfile >> s >> t;
  s--, t--;
  std::cout << distance(adj, s, t) << std::endl ;
}
