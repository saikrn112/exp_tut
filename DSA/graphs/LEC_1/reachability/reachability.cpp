#include <iostream>
#include <vector>
using namespace std;
using std::vector;
using std::pair;

int DFS(vector<vector<int> > &adj, int x, int y, vector<bool> &visited){
  if(x==y) return 1;
  visited[x] = true;
  int k = 0;
  for(auto i:adj[x]){
    //cout << i << " " << visited[i] << endl;
    if(visited[i] == false){
      k = DFS(adj, i, y, visited);
    }
    if(k == 1) return 1;
  }
  return 0;
}

int reach(vector<vector<int> > &adj, int x, int y) {
  //write your code here
  vector<bool> visited(adj.size(),false);
  return DFS(adj, x, y, visited);
}

int main() {
  size_t n, m;
  std::cin >> n >> m;
  vector<vector<int> > adj(n, vector<int>());
  for (size_t i = 0; i < m; i++) {
    int x, y;
    std::cin >> x >> y;
    adj[x - 1].push_back(y - 1);
    adj[y - 1].push_back(x - 1);
  }
  int x, y;
  std::cin >> x >> y;
  std::cout << reach(adj, x - 1, y - 1) << endl;
}
