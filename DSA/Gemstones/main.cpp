/* This code if for Gemstones Hackerrank */ 
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <algorithm>
#include <time.h>
using namespace std;

int main(){
	int n; // Number of Test cases
	int counter=0;
	ifstream myfile;
	map<char,int> gem;
	map<char,int>::iterator it;
	clock_t t_start =clock();   	
   	/* File Operations */
	myfile.open("TestCase2.txt");
	if(myfile.is_open())myfile>> n;
	else{
		cout << "Couldn't Open the file" << endl;
		return 1;
	}

	/* Algorithm for finding common elements in all rocks. This code was written with map data structure in mind*
	for(int j=1; j<=n; j++) {
		string s;	myfile >>s;
		sort(s.begin(),s.end());
		for(int i=0; i<s.size(); i++){
			it = gem.find(s[i]);
			if(it==gem.end() && j==1){
				gem[s[i]] = 1;
			}
			if(it!=gem.end() && gem[s[i]]==j-1){
				gem[s[i]] += 1;
			}
			if( it!=gem.end() && gem[s[i]]==n){
				gem[s[i]] += 1;
				// cout << s[i] << " " << gem[s[i]] << endl;
				counter++;
			}
		}
	}
	cout << counter << endl;

	/* Code from problem setters hackerrank*/
	/* This code is snippet is faster than the one I wrote */
    int i,ans=0,ar[109][26]={},j,flag;
    string s;
    for(i=0; i<n; i++)
    {
    myfile >> s;
    for(j=0; j<s.size(); j++)
        ar[i][s[j]-'a']++;
    }
    for(i=0; i<26; i++)
    {
    flag=0;
    for(j=0; j<n; j++)
        if(ar[j][i]==0)flag=1;
    if(flag==0)ans++;
    }
    cout << ans << endl;

	
	/* Printing time taken to run this program*/
	printf("Time Taken: %0.3f\n",(double)(clock() - t_start)/CLOCKS_PER_SEC);
	return 0;
}