/*This code is for Pairs in hackerrank */

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std; 

int main(){

	vector<int> arr;
	ifstream myfile
	int n;  
	int k;  
   	/* File Operations */
	myfile.open("TestCase1.txt");
	if(myfile.is_open())myfile>> n;
	else{
		cout << "Couldn't Open the file" << endl;
		return 1;
	}
	myfile >> k;
	for (int i=0; i<n; i++) myfile >> arr[i];
	sort(arr.begin(),arr.end());
	vector<int>::iterator l=arr.begin(); 
	/* This takes the vector and finds the pair such that difference between them is 'k'
	* Takes the input from a file
	* sorts it out 
	* using binary search checks if the value k+*l
	*/
	int counter=0;
	while(binary_search(l,arr.end(),*l+k)){
		counter++;
		l++;
	}
}