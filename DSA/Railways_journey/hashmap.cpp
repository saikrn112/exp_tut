/* This code is used for DSA assignment - 1 Question- 3 */
/* Following things are practised */
/* * Extracting information from a file and parsing it using getline
   * How to use 2 cpp files; Compiling them together and creating corresponding header file
   * using namespaces
*/
#include "printvector.h"
using namespace std;

int main() {
	/* Initialising the variable */
	int journey_time=0,time=0;
	queue<int> movie_times;
	string line,time_s;
	ifstream myfile;
	unordered_map<int,string> movies;
	std::unordered_map<int,string>::const_iterator got;


	/* File Operations */
	myfile.open("myfile.txt"); // opening the file
	if (myfile.is_open()) {
		getline(myfile,line);
		cout << line << endl;
		journey_time = stoi(line); // First element of the file : Number of movies
		cout << journey_time << endl;
	} else {
		cout << "file couldn't open" << endl;
		return 1;
	}

	/* Parsing the file and storing in the hashmap */
	while(getline(myfile,line,',')){
		// getline(myfile,line,',');
		getline(myfile,time_s);  time = stoi(time_s);
		movie_times.push(time);
		movies[time] = line;
		cout << line << "," << time << endl;
	}
	myfile.close();

	/* Algorithm for finding two movies whose sum of durations are equal to that of journey duration */
	/* * Algorithm is simple we have total duration i.e. 340mins
	   * We will subtracte this value with the duration of the movies given in the list
	   * We will get the required value which we will get from the hashtable O(1), if it exists we found the two movies required.
	*/
	while(!movie_times.empty()){
		 int temp_time = journey_time - movie_times.front();
		 got = movies.find(temp_time);
		 if(got == movies.end()){
		 	cout << " not found " << endl;
		 }
		 else {
		 	cout << "Found" << endl;
		 	cout << "Movies are " << movies[temp_time] << " and "<<movies[movie_times.front()] << endl;
		 	break;
		 }
		 movie_times.pop();
	}

	return 0;
}

/* Understood the following things from implmenting this code*/
/* * getline neatly parses the new line character in file and stores the value.
   * using the operator '>>' in the if condition in the code above doesnt parse the new line character and it is very inefficient
   * Hash map get function time complexity O(1) This is the average case that should be considered for algorithms
   * Worst case would be when hash is same for lot of elements which results in O(n), which is intuitive because if hash is same for every map
   * then it is as good array
*/
