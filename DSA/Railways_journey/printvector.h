#ifndef PRINTVECTOR_H
#define PRINTVECTOR_H
#include <iostream>
#include <fstream>
#include <queue>
#include <string>
#include <unordered_map>
#include <sstream>
namespace print{
	void printvector(std::vector<std::string> &);
	void add(int, int);
}
#endif