/* This code is a part of migratory birds challenge Hackerrank */
/* Algorithm is as follows 
* Store them in vector with datastructure which has value and index as members
* Find the highest valued index in the vector and 
* Check if there are two or more such index
* Print them 
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std; 

struct types{
	int index; 
	int value; 
};

bool mycomp(types a,types b){
	if(a.value==b.value) return a.index<b.index?true:false;
	else return a.value>b.value?true:false;
}

int main(){
	vector<types> arr; 
	arr.resize(5);
	ifstream myfile; 
	int n=0;
   	/* File Operations */
	myfile.open("TestCase.txt");
	if(myfile.is_open())myfile>> n;
	else{
		cout << "Couldn't Open the file" <<endl;
		return 1;
	}

	int k=0;
	for(int i=0; i<n; i++){
		myfile >> k;
		arr[k-1].index = k;
		arr[k-1].value++;
	}

	sort(arr.begin(),arr.end(),mycomp);
	cout << arr[0].index << endl;
	return 0;
}