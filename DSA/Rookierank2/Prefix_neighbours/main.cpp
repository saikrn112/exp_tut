/* This code is part of Rookierank2 Challenge Hackerrank 
* Challenge - Prefix Neighbours
* IMP - string.pop_back() not available in std c++99; It starts from std c++11
*/
/* This comment is for checking git stash */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std; 

struct string_val{
	string s; 
	int val; 
};

bool mycmp(string_val a, string_val b){
	if(a.s.size()==b.s.size()) return a.val<b.val?true:false; 
	return a.s.size()<b.s.size()?true:false;
}

void finder(vector<string_val>  strings){
	int max=0;
	vector<string> str;
	int i = strings.size()-1;	
	string s = strings[i].s;
	str.push_back(s);
	int flag=0;
	cout << s << endl;
	while(j>=0 ){
		if(s.find(strings[j].s)==string::npos){
			for(int i=0; i<str.size()-1; i++){
				if(str[i].find(strings[j].s)==string::npos)
				str.push_back(strings[j].s)
				value+=strings[j].val;
		}
		}else{
			flag=1;
		}
		j--;
	}

	max = value>max?value:max;

	cout << max << endl;
}

int main(){
	int n;
	vector<string_val> strings;

	/* File Operations */
	ifstream myfile; 
	myfile.open("TestCase.txt");
	if(myfile.is_open())myfile >> n;
	else {
		cout << "Couldn't Open the file" << endl;
		return 1;
	}
	strings.resize(n);

	/* Collecting data and storing them in data structure */
	int id=0;
	for(int i=0; i<n; i++){
		int sum=0;
		myfile >> strings[i].s;
		// cout << strings[i].s << " "; 
		for(int j=0; j<strings[i].s.size(); j++) sum+=int(strings[i].s[j]);
		// cout << sum << endl;
		strings[i].val = sum;
	}

	sort(strings.begin(), strings.end(),mycmp);
	for(int i=0; i<n; i++){
		cout << strings[i].s << " " << strings[i].val << endl;
	}cout << endl;

	finder(strings);

	return 0;
}