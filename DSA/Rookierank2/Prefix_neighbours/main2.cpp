/* This code is part of Rookierank2 Challenge Hackerrank 
* Challenge - Prefix Neighbours
* IMP - string.pop_back() not available in std c++99; It starts from std c++11
*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std; 

struct string_val{
	string s; 
	int val; 
};

bool mycmp(string_val a, string_val b){
	if(a.s.size()==b.s.size()) return a.val<b.val?true:false; 
	return a.s.size()<b.s.size()?true:false;
}

void finder(vector<string_val>  strings){
	int max=0;
	
	for(int i=strings.size()-1; i>1; i--){
		int value=strings[i].val;
		string s = strings[i].s;
		s.pop_back(); 
		int flag=0;
		int j=i-2;
		int flag2=0;
		cout << s << endl;
		while(s.size()!=0 && j>=0){
			if(flag==1){
				s.pop_back();
				cout << "flagging"<< endl;
				cout << s << endl;
			}
			if(s.size()<strings[j].s.size()){
				value+=strings[j].val;
				cout << strings[j].s << endl;
				j--;
			}
			else if(s.size()==strings[j].s.size()){
				cout << strings[j].s << endl;
				if(strings[j].s.find(s)==string::npos || flag2==1){
					cout << strings[j].s << endl;
					value+=strings[j].val;
					cout << value << endl;
					flag2=0;
				}
				else flag2=1;
				j--;
			}
			else{
				flag=1;
				// j--;
			}
		}
		max = value>max?value:max;
	}
	cout << max << endl;
}

int main(){
	int n;
	vector<string_val> strings;

	/* File Operations */
	ifstream myfile; 
	myfile.open("TestCase.txt");
	if(myfile.is_open())myfile >> n;
	else {
		cout << "Couldn't Open the file" << endl;
		return 1;
	}
	strings.resize(n);

	/* Collecting data and storing them in data structure */
	int id=0;
	for(int i=0; i<n; i++){
		int sum=0;
		myfile >> strings[i].s;
		// cout << strings[i].s << " "; 
		for(int j=0; j<strings[i].s.size(); j++) sum+=int(strings[i].s[j]);
		// cout << sum << endl;
		strings[i].val = sum;
	}

	sort(strings.begin(), strings.end(),mycmp);
	for(int i=0; i<n; i++){
		cout << strings[i].s << " " << strings[i].val << endl;
	}cout << endl;

	finder(strings);

	return 0;
}