/* This program is for insertion sort and testing its efficiency
* Take input from text file and runs the code 
@params filename
@params 
*/

#include <iostream>
#include <vector>
using namespace std;

void print(vector<int> v){
	for(auto i : v) cout << i << " ";
	cout << endl;
}

void insertionSort(vector<int> arr){
	for(int i=0; i<arr.size(); i++){
		auto tmp = arr[i+1];
		auto j = i+1;
		while(tmp>arr[j-1]&& j>=0){
			arr[j] = arr[j-1];			
			j--;
		}
		arr[j+1] = tmp;
		print(arr);
	}
}

int main(){
	   
    int _ar_size;
    cin >> _ar_size;
    int _ar[_ar_size], _ar_i;
    for(_ar_i = 0; _ar_i < _ar_size; _ar_i++)  cin >> _ar[_ar_i];
   	
   	insertionSort( _ar);
   
    
	return 0;
}