#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <climits>
#include <algorithm>
#include <list>

using std::pair;
using std::vector;



struct indexSteps {
  int id;
  int nSteps;
} typedef indexSteps;

indexSteps min_steps(indexSteps a,indexSteps b,indexSteps c){
  if(a.nSteps > b.nSteps){
    if(b.nSteps > c.nSteps) return c;
    else return b;
  } else if(a.nSteps > c.nSteps) return c;
  else return a;
}


indexSteps dP(int n, vector<vector<int>> &seq, vector<int> &numbers){
  // std::cout << n << " " << numbers[n] << std::endl;
  indexSteps a, b, c;
  a.id = n;
  a.nSteps = numbers[n];
  if(n==1 or numbers[n] != -1) return a;
  a.nSteps = INT_MAX;
  b.nSteps = INT_MAX;
  c.nSteps = INT_MAX;
  a = dP(n-1, seq, numbers);
  if(n%2==0) b = dP(n/2, seq, numbers);
  if(n%3==0) c = dP(n/3, seq, numbers);
  a = min_steps(a,b,c);
  // std::cout << n << " " << a.id << std::endl;
  seq[n].push_back(a.id);
  a.id = n;
  a.nSteps += 1;
  numbers[a.id] = a.nSteps;
  return a;
}

vector<int> optimal_sequence(int n) {
  vector<int> sequence;
  vector<int> numbers(n+1, -1);
  numbers[0] = 0;
  numbers[1] = 0;
  vector<vector<int>> optiSequence(n+1);
  dP(n,optiSequence, numbers);
  // std::cout << numbers[n] << std::endl;
  sequence.push_back(n);
  while(n > 1){
    sequence.push_back(optiSequence[n][0]);
    n = optiSequence[n][0];
  }
  reverse(sequence.begin(), sequence.end());
  return sequence;
}

int main() {
  int n;

  // std::ifstream myfile;

  /* File Operations */
  // myfile.open("myfile.txt"); // opening the file
  // if (myfile.is_open()) {
  //  //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
  //  myfile >> n ;
  // } else {
  //  std::cout << "file couldn't open" << std::endl;
  //  return 1;
  // }

  std::cin >> n;
  vector<int> sequence;
  vector<int> min_steps(n+1,0);
  vector<int> predecessors(n+1);
  std::iota(predecessors.begin()+1, predecessors.end(),0);
  for(int i = 2; i< n+1; i++){
    min_steps[i] = min_steps[i-1] + 1;
    if(i%2==0){
      if(min_steps[i] > min_steps[i/2] + 1){
        min_steps[i] = min_steps[i/2] + 1;
        predecessors[i] = i/2;
      }
    }
    if(i%3==0){
      if(min_steps[i] > min_steps[i/3] + 1){
        min_steps[i] = min_steps[i/3] + 1;
        predecessors[i] = i/3;
      }
    }
  }
  int tmp;
  tmp = n;
  sequence.push_back(tmp);
  while(tmp>1){
    tmp = predecessors[tmp];
    sequence.push_back(tmp);
  }
  reverse(sequence.begin(),sequence.end());
  std::cout << sequence.size() - 1 << std::endl;
  for(auto i:sequence) std::cout << i << " ";
  std::cout << std::endl;
}
