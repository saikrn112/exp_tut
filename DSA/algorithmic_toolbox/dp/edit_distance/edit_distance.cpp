#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using std::string;
using std::vector;

int edit_distance(const string &str1, const string &str2) {
  //write your code here
  vector<int> a(str2.size()+1,0);
  vector<vector<int>> value(str1.size()+1, a);
  int i = 0, j = 0;
  for(int j=0; j<= str2.size(); j++){
    value[i][j] = j;
  }
  j = 0;
  for(int i = 0; i<=str1.size(); i++){
    value[i][j] = i;
  }
  for(int i = 1; i <= str1.size(); i++){
    for(int j = 1; j <= str2.size(); j++){
      value[i][j] = value[i-1][j] + 1;
      if(value[i][j-1]+1 < value[i][j]) value[i][j] = value[i][j-1] + 1;
      if(str1[i-1] == str2[j-1]){
        if(value[i-1][j-1] < value[i][j]) value[i][j] = value[i-1][j-1];
      } else {
        if(value[i-1][j-1] + 1 < value[i][j]) value[i][j] = value[i-1][j-1] + 1;
      }
    }
  }

  // for(int i=0; i<value.size(); i++){
  //   for(auto j:value[i]) std::cout << j <<" ";
  //   std::cout << std::endl;
  // }

  return value[str1.size()][str2.size()];
}

int main() {
  string str1;
  string str2;

  // std::ifstream myfile;
  //
  // /* File Operations */
  // myfile.open("myfile.txt"); // opening the file
  // if (myfile.is_open()) {
  //  //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
  //  myfile >> str1 >> str2;
  // } else {
  //  std::cout << "file couldn't open" << std::endl;
  //  return 1;
  // }

  std::cin >> str1 >> str2;
  std::cout << edit_distance(str1, str2) << std::endl;
  return 0;
}
