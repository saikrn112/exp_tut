#include <iostream>
#include <vector>
#include <fstream>

using std::vector;

int optimal_weight(int W, const vector<int> &w) {
  //write your code here
  vector<int> a(W+1,0);
  vector<vector<int>> value(w.size()+1, a);
  for(int i=1; i<=w.size(); i++){
    for(int j=1; j<=W; j++){
      value[i][j] = value[i-1][j];
      if(w[i-1] <= j){
        int val = value[i-1][j-w[i-1]] + w[i-1];
        if(val > value[i][j]){
          value[i][j] = val;
        }
      }
    }
  }
  // for(int i=0; i<value.size(); i++){
  //   for(auto j:value[i]) std::cout << j <<" ";
  //   std::cout << std::endl;
  // }
  return value[w.size()][W];
}

int main() {
  int n, W;
  // std::ifstream myfile;
  //
  // /* File Operations */
  // myfile.open("myfile.txt"); // opening the file
  // if (myfile.is_open()) {
  //  //getline(myfile,line); n = std::stoi(line); // First element of the file : Number of workshops
  //  myfile >> W >> n;
  // } else {
  //  std::cout << "file couldn't open" << std::endl;
  //  return 1;
  // }

  std::cin >> W >> n;
  vector<int> w(n);
  for (int i = 0; i < n; i++) {
    // myfile >> w[i];
    std::cin >> w[i];
  }
  std::cout << optimal_weight(W, w) << '\n';
}
